-- CJSON
local cjson = require "cjson"
-- MONGODB
local mongo = require "resty.mongol"
local conn = nil
local db = nil

-- MONGO BSON
local bson = require('resty.mongol.bson')
local get_utc_date = bson.get_utc_date
-- HTTP
local http = require "resty.http"
-- REDIS
local redis = require "resty.redis"
local red = nil
-- MEMCACHED
local memcached = require "resty.memcached"
local memc = nil

-- GET DATE --
if tonumber(os.date("%H")) < 6 then
	tmpTime = os.time()-(60*60*24)
else
	tmpTime = os.time()
end

function strtotime(str)
	local pattern = "(%d+)-(%d+)-(%d+) (%d+):(%d+):(%d+)"
	local runyear, runmonth, runday, runhour, runminute, runseconds = str:match(pattern)
	return os.time({year = runyear, month = runmonth, day = runday, hour = runhour, min = runminute, sec = runseconds})
end

function get_conn_mongo()
	local conn = mongo:new()
	conn:set_timeout(1000)
	local ok, err = conn:connect("192.168.1.189",27017)
	if not ok then
		ngx.log(ngx.ERR, "error: ", err)
		ngx.exit(500)
	end
	return conn
end

function get_redis()
	local red = redis:new()
	red:set_timeout(1000) -- 1 sec
	local ok, err = red:connect("192.168.1.78", 6379)
	if not ok then
		ngx.log(ngx.ERR, "error: ", err)
		ngx.exit(500)
	end
	red:select(9)
	return red
end

function get_memcache()
	local memc, err = memcached:new()
	local ok, err = memc:connect("192.168.1.189", 11211)
	if not ok then
		ngx.log(ngx.ERR, "error: ", err)
		ngx.exit(500)
	end
	return memc
end

function log_db(command,action)
	--[[
	local log_file,err = io.open("/var/luacode/footballapi.kapook.com/log_db/" .. os.date("%Y-%m-%d") .. ".txt" , "a+")
	if not log_file then
		ngx.log(ngx.ERR, "error: ", err)
		ngx.exit(500)
	end
	local date_log = os.date("%Y-%m-%d %H:%M:%S")
	local tmp_data = { timestamp = date_log , command = command , action = action , url = ngx.var.uri }
	log_file:write(cjson.encode(tmp_data) .. "\n")
	log_file:close()
	]]--
end

function isint(n)
	return n==math.floor(n)
end

function explode(delimiter, text)
	local list = {}
	local pos = 1
	if string.find("", delimiter, 1) then
		-- We'll look at error handling later!
		error("delimiter matches empty string!")
	end
	--ngx.say(text)
	local i = 0
	while 1 do
		local first, last = string.find(text, delimiter, pos)
		--ngx.say (first, last)
		if first then
			list[i] = string.sub(text, pos, first-1)
			i = i +1
			pos = last+1
		else
			--table.insert(list, string.sub(text, pos))
			list[i] = string.sub(text, pos)
			break
		end
	end
	--ngx.say(list[1])
	return list
end

function implode(delimiter, array)
	local isFirst = true
	local return_str = ""
	for i in pairs(array) do
		if isFirst then
			isFirst = false
		else
			return_str = return_str .. ","
		end
		return_str = return_str .. array[i]
    end
	return return_str
end

function set_cache_redis(name_cache,time_cache,data_cache)
	if red == nil then
		red = get_redis()
	end
	local ok, err = red:set(name_cache,data_cache)
	if not ok then
		ngx.log(ngx.ERR, "error: ", err)
		return false
	end
	local ok, err = red:expire(name_cache,time_cache)
	if not ok then
		ngx.log(ngx.ERR, "error: ", err)
		return false
	end
	return true
end

function get_kapi_member(id)
	local httpc = http.new()
	local res, err = httpc:request_uri("http://kapi.kapook.com/profile/member/userid/"..id, {
		method = "GET",
		headers = {
			["Content-Type"] = "application/x-www-form-urlencoded",
		}
	})		
	if res then
		return_data = cjson.decode(res.body)
		if not return_data["data"] then
			return_data = { error_description = "Not Found" , status = -1 }
		end
	else
		ngx.log(ngx.ERR, "error: ", err)
		return_data = { error_description = err , status = -1 }
	end
	return return_data["data"]
end

function get_cookie(cookie_key)
	local list_str_cookie = explode(";",ngx.var.http_cookie)
	local index
	local item
	for index,item in pairs(list_str_cookie) do
		local attr	=	explode("=",item)
		attr[0] = string.gsub(attr[0], "%s+", "")
		if attr[0]==cookie_key then
			attr[1] = string.gsub(attr[1], "%\"+", "")
			return attr[1]
		end
	end
	return nil
end

function check_cookie_login()
	local hash = "kapook_sudyod"
	local uid = get_cookie("uid")
	local is_login = get_cookie("is_login")

	if uid and is_login then
		if ngx.md5(uid .. hash) == is_login then
			local expires = 172800
			ngx.header['Set-Cookie'] = "uid=" .. uid .. "; path=/; Expires=" .. ngx.cookie_time(ngx.time() + expires)
			ngx.header['Set-Cookie'] = "is_login=" .. is_login .. "; path=/; Expires=" .. ngx.cookie_time(ngx.time() + expires)
			return tonumber(uid)
        else
			return false
		end
	else
		return false
	end
end

function lastest_id(col_name,field_name)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col(col_name)
	local return_data	=	1
	local index
	local item
	
	local fullquery = {}
	fullquery["query"] = query
	fullquery["orderby"] = {}
	fullquery["orderby"][0] = {}
	fullquery["orderby"][0][field_name] = -1
						
	local cursor_id,r,t = collection:query( fullquery,{},0,1,{} )
	log_db("query","lastest_id")

	for index, item in pairs(r) do
		return item[field_name]+1
	end
	return return_data
end

function get_match(id,status,match_status)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_match")

	local time_date_from 	=	os.date("%Y-%m-%d 06:00:00",tmpTime)
	local time_date_to		=	os.date("%Y-%m-%d 05:59:59",tmpTime+(60*60*24))
	
	local query 			= {}
	
	query["id"] 			= id
	query["Status"] 		= status
	query["MatchStatus"] 	= match_status
	
	query["MatchDateTime"] 	= {}
	query["MatchDateTime"]["$gte"] 	= time_date_from
	query["MatchDateTime"]["$lte"] 	= time_date_to
	
	return_data = collection:find_one(query)
	log_db("find_one","get_match")
	
	return return_data
end

function get_match_by_xscore_id(xscore_id)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_match")
	query["XSMatchID"] 	= 	tonumber(xscore_id)
	
	return_data = collection:find_one(query)
	log_db("find_one","get_match_by_xscore_id")
	
	return return_data
end

function get_match_by_id(id)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_match")
	query["id"] 	= 	tonumber(id)
	
	return_data = collection:find_one(query)
	log_db("find_one","get_match_by_id")
	
	return return_data
end

function get_game_by_id(id)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_game")
	query["id"] 		= 	tonumber(id)
	
	return_data = collection:find_one(query)
	log_db("find_one","get_game_by_id")
	
	return return_data
end

function update_match_playside(id,playside)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_match")
	local query_set 	= 	{}
	query_set["$inc"] 	= 	{}
	query_set["$inc"]["count_playside_team" .. playside] = 1
	query_set["$inc"]["count_playside"] = 1
	local n, err = collection:update({ id = tonumber(id) },query_set,1)
	log_db("update","update_match_playside")
end

function get_member_data(id)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_member")
	
	return_data = collection:find_one({id = id})
	log_db("find_one","get_member_data")
	
	return return_data
end

function get_ranking_alltime(member_id)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_ranking_all")
	query["id"] 		= 	tonumber(member_id)

	return_data = collection:find_one(query)
	log_db("find_one","get_ranking_alltime")
	
	return return_data
end

function get_ranking_season(member_id,season)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_ranking_season")
	query["id"] 		= 	tonumber(member_id)
	query["season"] 	= 	tostring(season)
	
	return_data = collection:find_one(query)
	log_db("find_one","get_ranking_season")
	
	return return_data
end

function get_ranking_month(member_id,month)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_ranking_month")
	query["id"] 		= 	tonumber(member_id)
	query["month"] 		= 	tostring(month)
	
	return_data = collection:find_one(query)
	log_db("find_one","get_ranking_month")
	
	return return_data
end

function get_ranking_day(member_id,date_play)
	local query 		= 	{}
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_ranking_day")
	query["id"] 		= 	tonumber(member_id)
	query["date"] 		= 	tostring(date_play)
	
	return_data = collection:find_one(query)
	log_db("find_one","get_ranking_day")
	
	return return_data
end

function calculate_game(game_id,match_id,gameTypeBet,isFull)
	local item
	local isNotFinish = false
	
	local tmp_game_data = get_game_by_id(game_id)
	
	if tmp_game_data["status"] == 3 then
		ngx.say(tmp_game_data["id"] .. ":Finished<br>")
	end
	
	tmp_game_data["_id"] = nil
	for index, item in pairs(tmp_game_data["listGame"]) do
		if item["match_id"] == match_id and item["status"] == 1 then
			
			if type(item["isWDL"]) ~= "nil" then
				ngx.say(tmp_game_data["id"] .. ":Finished??????<br>")
				return
			end
			
			if gameTypeBet == 3 then
				item["isWDL"] = 0
			elseif gameTypeBet == item["team_win"] then
				if isFull then
					tmp_game_data["multiple"] = tmp_game_data["multiple"] * 2
					item["isWDL"] = 2
				else
					tmp_game_data["multiple"] = tmp_game_data["multiple"] * 1.5
					item["isWDL"] = 1
				end
			else
				if isFull then
					tmp_game_data["multiple"] = tmp_game_data["multiple"] * 0
					item["isWDL"] = -2
				else
					tmp_game_data["multiple"] = tmp_game_data["multiple"] * 0.5
					item["isWDL"] = -1
				end
			end
			item["status"] = 2
			tmp_game_data["listGame"][index] = item
		elseif item["status"] == 1 then
			isNotFinish = true
		end
	end
	if (tmp_game_data["multiple"] == 0 or isNotFinish == false) and tmp_game_data["status"] ~= 3 then
		tmp_game_data["point_return"] 	= 	(tmp_game_data["multiple"]*tmp_game_data["point"]) - tmp_game_data["point"]
		tmp_game_data["status"]			=	3
		local start_day = 1
		local start_month = 8
			
		-- add rate
		local inc_update = {
			total_play = 1,
			total_playside_point = math.ceil( ( tmp_game_data['point'] * tmp_game_data['multiple'] ) - tmp_game_data['point'] ),
			total_point = math.ceil( ( tmp_game_data['point'] * tmp_game_data['multiple'] ) - tmp_game_data['point'] )
		}
		if tonumber(tmp_game_data["match_id"]) > 0 then
			if tmp_game_data["multiple"] > 1.5 then
				inc_update["total_rate"] = 10
			elseif tmp_game_data["multiple"] > 1 then
				inc_update["total_rate"] = 5
			else
				inc_update["total_rate"] = 0
			end
		else
			if tmp_game_data["multiple"] > 1 then
				inc_update["total_rate"] = 10
			else
				inc_update["total_rate"] = 0
			end
		end
		
		if tmp_game_data["match_id"] > 0 then
			inc_update["total_playside_single"] = 1
			if tmp_game_data["multiple"] > 1.5 then
				inc_update["total_playside_single_win"] = 1
				inc_update["total_playside_single_win_full"] = 1
			elseif tmp_game_data["multiple"] > 1 then
				inc_update["total_playside_single_win"] = 1
				inc_update["total_playside_single_win_half"] = 1
			elseif tmp_game_data["multiple"] == 1 then
				inc_update["total_playside_single_draw"] = 1
			elseif tmp_game_data["multiple"] >= 0.5 then
				inc_update["total_playside_single_lose_half"] = 1
			else
				inc_update["total_playside_single_lose_full"] = 1
			end
		else
			inc_update["total_playside_multi"] = 1
			if tmp_game_data["multiple"] > 1 then
				inc_update["total_playside_multi_win"] = 1
			elseif tmp_game_data["multiple"] == 1 then
				inc_update["total_playside_multi_draw"] = 1
			else
				inc_update["total_playside_multi_lose_full"] = 1
			end
		end
		
		-- get a date
		local tmpYear,tmpMonth,tmpDay = tmp_game_data["date_play"]:match("(%d+)%-(%d+)%-(%d+)")
		
		-- YEARLY
		if tonumber(tmpMonth) > start_month or (tonumber(tmpMonth) == start_month and tonumber(tmpDay) >= start_day) then
			tmpSeasonData = get_ranking_season(tmp_game_data["user_id"],tonumber(tmpYear))
		else
			tmpSeasonData = get_ranking_season(tmp_game_data["user_id"],tonumber(tmpYear) - 1)
		end
		
		if tmpSeasonData ~= nil then
			tmpSeasonData["_id"] = nil
		else
			tmpSeasonData = {}
		end
		for key, val in pairs(inc_update) do
			if type(tmpSeasonData[key]) ~= "nil" then
				tmpSeasonData[key] = tmpSeasonData[key] + val
			else
				tmpSeasonData[key] = val
			end
		end
		
		if type(tmpSeasonData['total_playside_single_win']) == "nil" then
			tmpSeasonData['total_playside_single_win'] = 0
		end
		
		if type(tmpSeasonData['total_playside_single']) == "nil" then
			tmpSeasonData['total_playside_single'] = 0
		end
		
		if type(tmpSeasonData['total_playside_multi_win']) == "nil" then
			tmpSeasonData['total_playside_multi_win'] = 0
		end
		
		if type(tmpSeasonData['total_playside_multi']) == "nil" then
			tmpSeasonData['total_playside_multi'] = 0
		end
		
		if tmpSeasonData['total_playside_single'] > 0 then
			tmpSeasonData["total_playside_single_win_percent"] 	= (tmpSeasonData['total_playside_single_win']/tmpSeasonData['total_playside_single']) * 100.0
		else
			tmpSeasonData["total_playside_single_win_percent"] 	= 0.0
		end
		
		if tmpSeasonData['total_playside_multi'] > 0 then
			tmpSeasonData["total_playside_multi_win_percent"] 	= (tmpSeasonData['total_playside_multi_win']/tmpSeasonData['total_playside_multi']) * 100.0
		else
			tmpSeasonData["total_playside_multi_win_percent"] 	= 0.0
		end
		tmpSeasonData["total_rate_calculate"] 				= (tmpSeasonData['total_rate']/tmpSeasonData['total_play'])
		tmpSeasonData["total_rate_calculate_by_time"] 		= (tmpSeasonData['total_rate']/tmpSeasonData['total_play'])
		
		if conn == nil then
			conn = get_conn_mongo()
		end
		if db == nil then
			db = conn:new_db_handle ( "football" )
		end
	
		local col_season 	= 	db:get_col("football_ranking_season")
		local query_set 	= 	{}
		if tonumber(tmpMonth) > start_month or (tonumber(tmpMonth) == start_month and tonumber(tmpDay) >= start_day) then
			local n, err = col_season:update({ season = tostring(tmpYear), id = tonumber(tmp_game_data["user_id"]) },tmpSeasonData,1)
			log_db("update","calculate_game-season")
		else
			local n, err = col_season:update({ season = tostring(tmpYear - 1), id = tonumber(tmp_game_data["user_id"]) },tmpSeasonData,1)
			log_db("update","calculate_game-season")
		end
		
		
		-- Monthly
		tmpMonthlyData = get_ranking_month(tmp_game_data["user_id"],tmpYear .. "-" .. tmpMonth)
		
		if tmpMonthlyData ~= nil then
			tmpMonthlyData["_id"] = nil
		else
			tmpMonthlyData = {}
		end
		
		for key, val in pairs(inc_update) do
			if type(tmpMonthlyData[key]) ~= "nil" then
				tmpMonthlyData[key] = tmpMonthlyData[key] + val
			else
				tmpMonthlyData[key] = val
			end
		end
		
		if type(tmpMonthlyData['total_playside_single_win']) == "nil" then
			tmpMonthlyData['total_playside_single_win'] = 0
		end
		
		if type(tmpMonthlyData['total_playside_single']) == "nil" then
			tmpMonthlyData['total_playside_single'] = 0
		end
		
		if type(tmpMonthlyData['total_playside_multi_win']) == "nil" then
			tmpMonthlyData['total_playside_multi_win'] = 0
		end
		
		if type(tmpMonthlyData['total_playside_multi']) == "nil" then
			tmpMonthlyData['total_playside_multi'] = 0
		end
		
		if tmpMonthlyData['total_playside_single'] > 0 then
			tmpMonthlyData["total_playside_single_win_percent"] 	= (tmpMonthlyData['total_playside_single_win']/tmpMonthlyData['total_playside_single']) * 100.0
		else
			tmpMonthlyData["total_playside_single_win_percent"] 	= 0.0
		end
		
		if tmpMonthlyData['total_playside_multi'] > 0 then
			tmpMonthlyData["total_playside_multi_win_percent"] 	= (tmpMonthlyData['total_playside_multi_win']/tmpMonthlyData['total_playside_multi']) * 100.0
		else
			tmpMonthlyData["total_playside_multi_win_percent"] 	= 0.0
		end
		tmpMonthlyData["total_rate_calculate"] 				= 	tmpSeasonData["total_rate_calculate"] 
		tmpMonthlyData["total_rate_calculate_by_time"] 		= 	(tmpMonthlyData['total_rate']/tmpMonthlyData['total_play'])
		
		local col_month 	= 	db:get_col("football_ranking_month")
		local query_set 	= 	{}
		local n, err = col_month:update({ month = tmpYear .. "-" .. tmpMonth, id = tonumber(tmp_game_data["user_id"]) },tmpMonthlyData,1)
		log_db("update","calculate_game-month")
		
		-- Daily
		tmpDailyData = get_ranking_day(tmp_game_data["user_id"],tmp_game_data["date_play"])
		
		if tmpDailyData ~= nil then
			tmpDailyData["_id"] = nil
		else
			tmpDailyData = {}
		end
		
		for key, val in pairs(inc_update) do
			if type(tmpDailyData[key]) ~= "nil" then
				tmpDailyData[key] = tmpDailyData[key] + val
			else
				tmpDailyData[key] = val
			end
		end
		
		if type(tmpDailyData['total_playside_single_win']) == "nil" then
			tmpDailyData['total_playside_single_win'] = 0
		end
		
		if type(tmpDailyData['total_playside_single']) == "nil" then
			tmpDailyData['total_playside_single'] = 0
		end
		
		if type(tmpDailyData['total_playside_multi_win']) == "nil" then
			tmpDailyData['total_playside_multi_win'] = 0
		end
		
		if type(tmpDailyData['total_playside_multi']) == "nil" then
			tmpDailyData['total_playside_multi'] = 0
		end
		
		if tmpDailyData['total_playside_single'] > 0 then
			tmpDailyData["total_playside_single_win_percent"] 	= (tmpDailyData['total_playside_single_win']/tmpDailyData['total_playside_single']) * 100.0
		else
			tmpDailyData["total_playside_single_win_percent"] 	= 0.0
		end
		
		if tmpDailyData['total_playside_multi'] > 0 then
			tmpDailyData["total_playside_multi_win_percent"] 	= (tmpDailyData['total_playside_multi_win']/tmpDailyData['total_playside_multi']) * 100.0
		else
			tmpDailyData["total_playside_multi_win_percent"] 	= 0.0
		end
		tmpDailyData["total_rate_calculate"] 				= 	tmpSeasonData["total_rate_calculate"] 
		tmpDailyData["total_rate_calculate_by_time"] 		= 	(tmpDailyData['total_rate']/tmpDailyData['total_play'])
		
		local col_day 	= 	db:get_col("football_ranking_day")
		local query_set 	= 	{}
		local query_con 	=	{ id = tonumber(tmp_game_data["user_id"]) }
		query_con["date"]	=	tmp_game_data["date_play"]
		
		local n, err = col_day:update(query_con,tmpDailyData,1)
		log_db("update","calculate_game-day")
		
		-- ALL Time
		tmpAllTimeData = get_ranking_alltime(tmp_game_data["user_id"])
		
		if tmpAllTimeData ~= nil then
			tmpAllTimeData["_id"] = nil
		else
			tmpAllTimeData = {}
		end
		
		for key, val in pairs(inc_update) do
			if type(tmpAllTimeData[key]) ~= "nil" then
				tmpAllTimeData[key] = tmpAllTimeData[key] + val
			else
				tmpAllTimeData[key] = val
			end
		end
		
		if type(tmpAllTimeData['total_playside_single_win']) == "nil" then
			tmpAllTimeData['total_playside_single_win'] = 0
		end
		
		if type(tmpAllTimeData['total_playside_single']) == "nil" then
			tmpAllTimeData['total_playside_single'] = 0
		end
		
		if type(tmpAllTimeData['total_playside_multi_win']) == "nil" then
			tmpAllTimeData['total_playside_multi_win'] = 0
		end
		
		if type(tmpAllTimeData['total_playside_multi']) == "nil" then
			tmpAllTimeData['total_playside_multi'] = 0
		end
		
		if tmpAllTimeData['total_playside_single'] > 0 then
			tmpAllTimeData["total_playside_single_win_percent"] 	= (tmpAllTimeData['total_playside_single_win']/tmpAllTimeData['total_playside_single']) * 100.0
		else
			tmpAllTimeData["total_playside_single_win_percent"] 	= 0.0
		end
		
		if tmpAllTimeData['total_playside_multi'] > 0 then
			tmpAllTimeData["total_playside_multi_win_percent"] 	= (tmpAllTimeData['total_playside_multi_win']/tmpAllTimeData['total_playside_multi']) * 100.0
		else
			tmpAllTimeData["total_playside_multi_win_percent"] 	= 0.0
		end
		tmpAllTimeData["total_rate_calculate"] 				= 	tmpSeasonData["total_rate_calculate"] 
		tmpAllTimeData["total_rate_calculate_by_time"] 		= 	(tmpAllTimeData['total_rate']/tmpAllTimeData['total_play'])
		
		local col_alltime 	= 	db:get_col("football_ranking_all")
		local query_set 	= 	{}
		local n, err = col_alltime:update({ id = tonumber(tmp_game_data["user_id"]) },tmpAllTimeData,1)
		log_db("update","calculate_game-alltime")

		-- CURRENT Point
		if os.date("%Y-%m",tmpTime) == tostring(tmpYear .. "-" .. tmpMonth) then
			local col_member 	= 	db:get_col("football_member")
			local query_set 	= 	{}
			query_set["$inc"] = { point = math.ceil((tmp_game_data["point"]*tmp_game_data["multiple"])-tmp_game_data["point"]) }
			local n, err = col_member:update({ id = tonumber(tmp_game_data["user_id"]) },query_set,1)
			log_db("update","calculate_game-member")
		end
		
	elseif tmp_game_data["status"] < 2 then
		tmp_game_data["status"]			=	2
	end
	
	if memc == nil then
		memc = get_memcache()
	end
	local ok, err = memc:delete("Football2014-GameData-" .. tmp_game_data["user_id"])

	-- Update Gameinfo to MongoDB
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local col_game 	= 	db:get_col("football_game")
	local query_set 	= 	{}
	query_set["$set"] 	= 	tmp_game_data
	local n, err = col_game:update({ id = tonumber(tmp_game_data["id"]) },query_set,1)
	log_db("update","calculate_game-game")
	
	--ngx.say(cjson.encode(tmp_game_data))
	--ngx.say("<br>")
end

function calculate_game_test(game_id,match_id,gameTypeBet,isFull)
	local item
	local isNotFinish = false
	local start_day = 1
	local start_month = 8
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local tmp_game_data = get_game_by_id(game_id)
	
	tmp_game_data["_id"] = nil

	-- get a date
	local tmpYear,tmpMonth,tmpDay = tmp_game_data["date_play"]:match("(%d+)%-(%d+)%-(%d+)")
		
	-- YEARLY
	if tonumber(tmpMonth) > start_month or (tonumber(tmpMonth) == start_month and tonumber(tmpDay) >= start_day) then
		tmpSeasonData = get_ranking_season(tmp_game_data["user_id"],tonumber(tmpYear))
	else
		tmpSeasonData = get_ranking_season(tmp_game_data["user_id"],tonumber(tmpYear) - 1)
	end
		
	if tmpSeasonData ~= nil then
		tmpSeasonData["_id"] = nil
	else
		tmpSeasonData = {}
	end

	local col_season 	= 	db:get_col("football_ranking_season_tmp")
	if tonumber(tmpMonth) > start_month or (tonumber(tmpMonth) == start_month and tonumber(tmpDay) >= start_day) then
		local n, err = col_season:update({ season = tostring(tmpYear), id = tonumber(tmp_game_data["user_id"]) },tmpSeasonData,1)
		log_db("update","calculate_game-season")
	else
		local n, err = col_season:update({ season = tostring(tmpYear - 1), id = tonumber(tmp_game_data["user_id"]) },tmpSeasonData,1)
		log_db("update","calculate_game-season")
	end
		
		
	-- Monthly
	tmpMonthlyData = get_ranking_month(tmp_game_data["user_id"],tmpYear .. "-" .. tmpMonth)

	if tmpMonthlyData ~= nil then
		tmpMonthlyData["_id"] = nil
	else
		tmpMonthlyData = {}
	end
		
	local col_month 	= 	db:get_col("football_ranking_month_tmp")
	local n, err = col_month:update({ month = tmpYear .. "-" .. tmpMonth, id = tonumber(tmp_game_data["user_id"]) },tmpMonthlyData,1)
	log_db("update","calculate_game-month")
		
	-- Daily
	tmpDailyData = get_ranking_day(tmp_game_data["user_id"],tmp_game_data["date_play"])
		
	if tmpDailyData ~= nil then
		tmpDailyData["_id"] = nil
	else
		tmpDailyData = {}
	end
		
	local col_day 	= 	db:get_col("football_ranking_day_tmp")
	local query_con 	=	{ id = tonumber(tmp_game_data["user_id"]) }
	query_con["date"]	=	tmp_game_data["date_play"]
		
	local n, err = col_day:update(query_con,tmpDailyData,1)
	log_db("update","calculate_game-day")
		
	-- ALL Time
	tmpAllTimeData = get_ranking_alltime(tmp_game_data["user_id"])
		
	if tmpAllTimeData ~= nil then
		tmpAllTimeData["_id"] = nil
	else
		tmpAllTimeData = {}
	end
		
	local col_alltime 	= 	db:get_col("football_ranking_all_tmp")
	local n, err = col_alltime:update({ id = tonumber(tmp_game_data["user_id"]) },tmpAllTimeData,1)
	log_db("update","calculate_game-alltime")

	-- CURRENT Point
	if os.date("%Y-%m",tmpTime) == tostring(tmpYear .. "-" .. tmpMonth) then
		local col_member 	= 	db:get_col("football_member_tmp")
		local query_set 	= 	{}
		query_set["$inc"] = { point = 0 }
		local n, err = col_member:update({ id = tonumber(tmp_game_data["user_id"]) },query_set,1)
		log_db("update","calculate_game-member")
	end
	
	if memc == nil then
		memc = get_memcache()
	end
	local ok, err = memc:delete("Football2014-GameData-" .. tmp_game_data["user_id"])

	-- Update Gameinfo to MongoDB
	local col_game 	= 	db:get_col("football_game_tmp")
	local query_set 	= 	{}
	query_set["$set"] 	= 	tmp_game_data
	local n, err = col_game:update({ id = tonumber(tmp_game_data["id"]) },query_set,1)
	log_db("update","calculate_game-game")
	
end

function json_say(data,callback)
	if type(callback) == "string" then
		return callback .. "(" .. cjson.encode(data) .. ")"
	else
		return cjson.encode(data)
	end
end

function get_scorer(match_id)
	local httpc = http.new()
	local res, err = httpc:request_uri("http://144.76.71.116/rpc?proc=scorers&param=%271|" .. match_id .. "|soccer%27", {
		method = "GET",
		headers = {
			["Content-Type"] = "application/x-www-form-urlencoded",
		}
	})
	if res then
		ngx.say(res.body)
		local a = string.sub (string.match(res.body,"\".+\""), 2, -2)
		local temp_data = explode("&",a)
		--ngx.say(cjson.encode(arr_data))
		-- use arr_data[0]
		list_scorer = explode("|",temp_data[0])
		local arr_scorer = {}
		local arr_str_scorer = {}
		for index, item in pairs(list_scorer) do
			local data_scorer = explode("-",item)
			if table.getn(data_scorer) == 2 then
				for i_data,item_data in pairs(arr_scorer) do
					if item_data["team_id"] == data_scorer[1] then
						if item_data["note"] == "Own" then
							table.insert(arr_str_scorer,item_data["name"] .. "(" .. item_data["note"] .. item_data["min"] .. ")" )
						else
							table.insert(arr_str_scorer,"(" .. item_data["note"] .. item_data["min"] .. ")" .. item_data["name"] )
						end
					else
						if item_data["note"] == "Own" then
							table.insert(arr_str_scorer,"(" .. item_data["note"] .. item_data["min"] .. ")" .. item_data["name"] )
						else
							table.insert(arr_str_scorer,item_data["name"] .. "(" .. item_data["note"] .. item_data["min"] .. ")" )
						end
					end
				end
				break
			elseif table.getn(data_scorer) >= 4 then
				local index_data = table.getn(data_scorer)-4
				local tmp_scorer = {}
				if tonumber(data_scorer[1+index_data]) <= 3 then
					for i_data,item_data in pairs(data_scorer) do
						if tonumber(i_data) > index_data then
							break
						end
						if type(tmp_scorer["name"]) == "nil" then
							tmp_scorer["name"] = item_data
						else
							tmp_scorer["name"] = tmp_scorer["name"] .. "-" .. item_data
						end
					end
					tmp_scorer["period"] = data_scorer[1+index_data]
					tmp_scorer["team_id"] = data_scorer[2+index_data]
					tmp_scorer["min"] = data_scorer[3+index_data]
					tmp_scorer["note"] = data_scorer[4+index_data]
					table.insert(arr_scorer,tmp_scorer)
				end
			end
		end
		for index,item in pairs(arr_str_scorer) do
			arr_str_scorer[index-1] = arr_str_scorer[index]
			if tonumber(index) == table.getn(arr_str_scorer) then
				arr_str_scorer[index] = nil
			end
		end
		return arr_str_scorer
	else
		return {}
	end
end

local data_arr	=	explode('/',ngx.var.uri)

if data_arr[2] == "test" then
	ngx.header["Content-Type"] = "application/json; charset=utf-8"
	--[[if memc == nil then
		memc = get_memcache()
	end
	local res, flags, err = memc:get("Football2014-ListGameID-" .. data_arr[3])
	local data = {data = res}
	if err then
		ngx.say("failed to get dog: ", err)
		return
	end
	ngx.say(cjson.encode(data))]]--
	--ngx.say(ngx.unescape_uri("%E0%B8%99%E0%B8%9E%E0%B8%9E%E0%B8%A3/test/some%20string?foo=bar"))
	--return 
elseif data_arr[2] == "insert" then
	ngx.header["Content-Type"] = "application/json; charset=utf-8"
	local callback_str = nil
	if type(ngx.var['arg_callback']) ~= "nil" then
		callback_str = ngx.var['arg_callback']
	end

	local present_single = 0
	local present_multiple = 0
	local used_point = 0
	
	local quota_playside_single		=	3;
	local quota_playside_multi		=	1;
	local qouta_playside_multi_min	=	2;
	local qouta_playside_multi_max	=	6;
	
	uid = check_cookie_login()
	
	if uid == false then
		ngx.say(json_say({
			code_id = 400,
			message = "Authentication is not success."
		},callback_str))
		return
	end
	
	data_member = get_member_data(uid)
	if data_member == nil then
		ngx.say(json_say({
			code_id = 2,
			message = "Point has not enough."
		},callback_str))
		return
	end
	
	ngx.req.read_body()
	local args, err = ngx.req.get_uri_args()
	if not args then
		ngx.say(json_say({
			code_id = 0,
			message = "failed to get post args: ", err
		},callback_str))
		return
	end
	
	local listMatch = {}
	local listGame = {}
	local size_match = 0
	
	for key, val in pairs(args) do
		if string.find(key,"team_win%[") then
			
			local s,e = string.find(key,"team_win%[")
			key = string.sub (key, e+1)
			
			local s,e = string.find(key,"%]")
			key = string.sub (key, 0 , s-1)
			
			if tonumber(val) ~= 1 and tonumber(val) ~= 2 then
				ngx.say(json_say({
					code_id =	6,
					message =	"number team select is wrong.",
					val = tonumber(val)
				},callback_str))
				return
			end
			
			listMatch[tonumber(key)] = tonumber(val)
			size_match = size_match + 1
			
		elseif key == "isSingle" then
			if tonumber(val) == 1 then
				isSingle = true
			else
				isSingle = false
			end
		elseif key == "point" then
			point = tonumber(val)
			if point < 0 then
				ngx.say(json_say({
					code_id =	1,
					message =	"point must over 0."
				},callback_str))
				return
			end
		end
	end

	if type(isSingle) == "nil" then
		isSingle = false
	end

	if type(point) ~= "number" then
		ngx.say(json_say({
			code_id =	1,
			message =	"point must Number."
		},callback_str))
		return
	end
	
	if isint(point) == false then
		ngx.say(json_say({
			code_id =	1,
			message =	"point must Number."
		},callback_str))
		return
	end

	if point <= 0 then
		ngx.say(json_say({
			code_id =	1,
			message =	"point must over 0."
		},callback_str))
		return
	end

	if size_match ~= 1 and isSingle then
		ngx.say(json_say({
			code_id =	3,
			message =	"Input is not Single,but you want single."
		},callback_str))
		return
	end
	
	if size_match < qouta_playside_multi_min and isSingle==false then
		ngx.say(json_say({
			code_id =	4,
			message =	"Input less minimum quota."
		},callback_str))
		return
	end
	
	if size_match > qouta_playside_multi_max and isSingle==false then
		ngx.say(json_say({
			code_id =	12,
			message =	"Input is over qouta for Multi."
		},callback_str))
		return
	end
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local collection 	= 	db:get_col("football_game")
	local query 		= 	{ date_play = os.date("%Y-%m-%d",tmpTime),user_id = uid }
	local fullquery 	= 	{ query = query }				
	local cursor_id,r,t = 	collection:query( fullquery,{},0,10,{} )
	log_db("query","search-game")
	for index, item in pairs(r) do
		if item["match_id"] < 0 then
			present_multiple 	= 	present_multiple+1
		else
			present_single 		= 	present_single+1
		end
		if item["status"] == 1 or item["status"] == 2 then
			used_point = used_point + item["point"]
		end
	end
	
	if (data_member["point"] - used_point) < point  then
		ngx.say(json_say({
			code_id = 2,
			message = "Point has not enough."
		},callback_str))
		return
	end
	
	if present_multiple >= quota_playside_multi and isSingle == false then
		ngx.say(json_say({
			code_id = 11,
			message = "Quota of Multi-playside is full."
		},callback_str))
		return
	end
	
	if present_single >= quota_playside_single and isSingle == true then
		ngx.say(json_say({
			code_id = 9,
			message = "Quota of single-playside is full."
		},callback_str))
		return
	end

	local tmpMatchID
	local index = 0
	for match_id, sideplay in pairs(listMatch) do
		
		local tmp_match = get_match( match_id, 1, "Sched" )
		
		if tmp_match == nil then
			ngx.say(json_say({
				code_id =	7,
				message =	"Not Found a Match(Match No." .. match_id .. ")."
			},callback_str))
			return
		else
			if memc == nil then
				memc = get_memcache()
			end
			listGame[index] = {
				match_id 				= 	tonumber(match_id),
				team_win 				= 	tonumber(sideplay),
				status 					=	1,
				team_win_nameTHShort	= 	memc:get("Football2014-Team-NameTHShort-" .. tmp_match["Team" .. sideplay .. "KPID"] )
			}
			tmp_match["_id"] = nil
			local tmp_odds_data = {
				Odds = tmp_match["Odds"],
				id = tmp_match["id"],
				XSMatchID = tmp_match["XSMatchID"]
			}
			if type(tmp_match["TeamOdds"]) ~= "nil" then
				tmp_odds_data["TeamOdds"] = tmp_match["TeamOdds"]
			end
			set_cache_redis("MatchGameInfo-XScoreID-" .. tmp_match["XSMatchID"],60*60*24,cjson.encode(tmp_odds_data))

		end
		tmpMatchID = match_id
		index = index + 1
	end
	
	if isSingle == false then
		tmpMatchID = -1
	end
	
	if memc == nil then
		memc = get_memcache()
	end
	local ok, err = memc:delete("Football2014-game-playside-list-info-" .. uid .. "-" .. os.date("%Y-%m-%d",tmpTime))
	
	local game_id = lastest_id("football_game","id")
	local n, err = collection:insert({{
		id = game_id,
		date_play = os.date("%Y-%m-%d",tmpTime),
		listGame = listGame,
		user_id = uid,
		status = 1,
		point = point,
		point_return = 0,
		multiple = 1.0,
		game_type = 2,
		timestamp = os.date("%Y-%m-%d %H:%M:%S"),
		match_id = tmpMatchID
	}})
	log_db("insert","save-game")
	
	for match_id, sideplay in pairs(listMatch) do
	
		-- Write data to CSV------
		local dataplay_file,err = io.open("/var/luacode/footballapi.kapook.com/play_db/" .. os.date("%Y-%m-%d") .. ".txt" , "a+")
		if not dataplay_file then
			ngx.log(ngx.ERR, "error: ", err)
			ngx.exit(500)
		end
		local date_log = os.date("%Y-%m-%d %H:%M:%S")
		dataplay_file:write(uid .. ',' .. date_log .. ',' .. sideplay .. ',' .. match_id .. ',' .. os.date("%Y-%m-%d",tmpTime) .. '\n')
		dataplay_file:close()
		----------------------------
		
		if red == nil then
			red = get_redis()
		end
		local tmp_data, err = red:get("match_" .. match_id)
		if type(tmp_data) ~= "string" then
			tmp_data = {}
		else
			tmp_data = cjson.decode(tmp_data)
		end
		table.insert(tmp_data,game_id)
		set_cache_redis("match_" .. match_id,60*60*24,cjson.encode(tmp_data))
		
		update_match_playside(match_id,sideplay)
		
	end
	
	set_cache_redis("gameinfo_" .. game_id,60*60*24,cjson.encode({
		id = game_id,
		date_play = os.date("%Y-%m-%d",tmpTime),
		listGame = listGame,
		user_id = uid,
		status = 1,
		point = point,
		point_return = 0,
		multiple = 1.0,
		game_type = 2,
		timestamp = os.date("%Y-%m-%d %H:%M:%S"),
		match_id = tmpMatchID
	}))
	
	ngx.say(json_say({
		code_id =	200,
		message =	"Insert data successful.",
		id = game_id,
		uid = uid,
		point_remain = data_member["point"] - used_point - point,
		point = point,
		isSingle = isSingle,
		data = listGame
	},callback_str))
	local ok, err = memc:delete("Football2014-GameData-" .. uid)
	
	return
	

elseif data_arr[2] == "update_from_vpn" then
	ngx.header["Content-Type"] = "text/html; charset=utf-8"
	-- VPN Singapore
	conn_vpn = mongo:new()
	conn_vpn:set_timeout(10000)
	ok, err = conn_vpn:connect("198.211.108.80",27017)
	if not ok then
		ngx.say("connect failed1: "..err)
	end
	
	local db_vpn 	= 	conn_vpn:new_db_handle( "football_xscore_feed" )
	local col_vpn 	= 	db_vpn:get_col("football_match_xscore")
	-- Local Match
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local col_match 	= 	db:get_col("football_match")
	
	if type(ngx.var['arg_date']) ~= "nil" then
		tmpDate = ngx.var['arg_date']
		tmpYear,tmpMonth,tmpDay = tmpDate:match("(%d+)%-(%d+)%-(%d+)")
		tmpDateTime = os.time{year=tmpYear,month=tmpMonth,day=tmpDay}
		
		MatchDateStart = tmpDate .. " 06:00:00"
		MatchDateEnd = os.date("%Y-%m-%d 05:59:59",tmpDateTime+(60*60*24))
	else
		MatchDateStart = os.date("%Y-%m-%d %H:%M:%S",os.time()-(60*60*4))
		MatchDateEnd = os.date("%Y-%m-%d %H:%M:%S",os.time()+(60*60))
	end
	
	ngx.say( 'CHECK BETWEEN => ' .. MatchDateStart .. ' => ' .. MatchDateEnd )
	ngx.say( '<hr>' )
	
	--[[ VPS -> Cache&Local ]]--
	local query 		= 	{ MatchDateTime = {} }
	query["MatchDateTime"]["$gte"] = MatchDateStart
	query["MatchDateTime"]["$lte"] = MatchDateEnd
	local fullquery 	= 	{ query = query }
	local cursor_id,r,t = 	col_vpn:query( fullquery,{},0,1000,{} )
	local match_cal		=	50
	for index, item in pairs(r) do
		
		local tmp_data = {}
		local gameTypeBet = 0
		
		---------------------- UPDATE MATCH CACHE --------------------------
		tmp_data["SourceDate"]		=	item["SourceDate"]
		tmp_data["SourceTime"]		=	item["SourceTime"]
		tmp_data["MatchDateTime"]	=	item["MatchDateTime"]
		tmp_data["MatchStatus"]		=	item['MatchStatus']
		tmp_data["Team1YC"]			=	tonumber(item["Team1YC"])
		tmp_data["Team2YC"]			=	tonumber(item["Team2YC"])
		tmp_data["Team1RC"]			=	tonumber(item["Team1RC"])
		tmp_data["Team2RC"]			=	tonumber(item["Team2RC"])
		tmp_data["HTScore"]			=	item["HTScore"]
		tmp_data["FTScore"]			=	item["FTScore"]
		tmp_data["ETScore"]			=	item["ETScore"]
		tmp_data["PNScore"]			=	item["PNScore"]
		tmp_data["Team1FTScore"]	=	tonumber(item["Team1FTScore"])
		tmp_data["Team2FTScore"]	=	tonumber(item["Team2FTScore"])
		tmp_data["Minute"]			=	item["Minute"]
		if tmp_data["MatchStatus"] == "1 HF" or tmp_data["MatchStatus"] == "2 HF" or tmp_data["MatchStatus"] == "H/T" or tmp_data["MatchStatus"] == "E/T" or tmp_data["MatchStatus"] == "Pen" or tmp_data["MatchStatus"] == "Fin" then
			tmp_data["LastScorers"]		=	get_scorer(item["XSMatchID"])
			item["LastScorers"]		=	tmp_data["LastScorers"]
		end
		tmp_data["LastUpdate"]		=	os.date("Y-m-d H:i:s")
		tmp_data["LastUpdateMongo"] =	get_utc_date(ngx.time()*1000)
		---------------------- UPDATE MATCH DB --------------------------
		local query_set = {}
		query_set["$set"] = tmp_data
		local n, err = col_match:update({ XSMatchID = item['XSMatchID'], AutoMode = 1 },query_set)
		log_db("update","update_from_vpn")
		
		--[[ START:Log for after 04.00 ]]--
		
		if tonumber(os.date("%H")) >= 4 and tonumber(os.date("%H")) <= 6 then
			local dataplay_file,err = io.open("/var/luacode/footballapi.kapook.com/match_db/" .. os.date("%Y-%m-%d") .. ".txt" , "a+")
			if not dataplay_file then
				ngx.log(ngx.ERR, "error: ", err)
				ngx.exit(500)
			end
			local date_log = os.date("%Y-%m-%d %H:%M:%S")
			dataplay_file:write(date_log .. '-XSMatchID-' .. item['XSMatchID'] .. ':' .. item['MatchStatus'] .. ':' .. item['Minute'] .. ':' .. item["HTScore"] .. ':' .. item["FTScore"] .. ':' .. item["ETScore"] .. ':' .. item["PNScore"] .. '\n')
			dataplay_file:close()
		end
		
		--[[ END:Log for after 04.00 ]]--
		
		ngx.say('XSMatchID : ' .. item['XSMatchID'])
		ngx.say(' => ' .. item['Team1'] .. '  <font color="#990000">' .. item['Team1FTScore'] .. ' - ' .. item['Team2FTScore'] .. '</font>  ' .. item['Team2'])
		ngx.say('<br>Match Date : ' .. item['MatchDateTime'])
		ngx.say('<br>Match Status : ' .. item['MatchStatus'])
		ngx.say('  (' .. item['Minute'] .. '") ')
		ngx.say('<br>')
		ngx.say('<br>')
		
		if red == nil then
			red = get_redis()
		end
		local tmp_data_MatchGameInfo, err = red:get("MatchGameInfo-XScoreID-" .. item["XSMatchID"])
		local match_id = -1
		
		if type(tmp_data_MatchGameInfo) == "string" then
			tmp_data_MatchGameInfo = cjson.decode(tmp_data_MatchGameInfo)
			match_id = tmp_data_MatchGameInfo['id']
		else
			match_data = get_match_by_xscore_id(item["XSMatchID"])
			if match_data ~= nil then
				match_id = match_data['id']
			end
			
			local tmp_odds_data = {
				id = match_id,
				XSMatchID = item["XSMatchID"]
			}
			set_cache_redis("MatchGameInfo-XScoreID-" .. item["XSMatchID"],60*60*24,cjson.encode(tmp_odds_data))
		end

		if match_id > 0 then
			-- unset "_id"
			item["_id"] = nil
					
			-- Setting Time
			item["CreateDateMongo"] = { sec = math.floor(item["CreateDateMongo"]/1000) , usec = math.fmod(item["CreateDateMongo"],1000)*1000 }
			item["MatchDateTimeMongo"] = { sec = math.floor(item["MatchDateTimeMongo"]/1000) , usec = math.fmod(item["MatchDateTimeMongo"],1000)*1000 }
			item["LastUpdateMongo"] = { sec = math.floor(item["LastUpdateMongo"]/1000) , usec = math.fmod(item["LastUpdateMongo"],1000)*1000 }

			-- Record update time
			item["updatetime_cache"] = os.date("%Y-%m-%d %H:%M:%S")
					
			-- Save to Memcache
			if memc == nil then
				memc = get_memcache()
			end
			memc:set("Football2016-MatchInfo-" .. match_id, cjson.encode(item),3600)
		end

	end
	ngx.say( 'CHECK BETWEEN => ' .. MatchDateStart .. ' => ' .. MatchDateEnd .. '|AutoMode = 0')
	ngx.say( '<hr>' )
	--[[ Local[AutoMode = 0 && Time] -> Cache ]]--
	local query 		= 	{ MatchDateTime = {},AutoMode = 0 }
	query["MatchDateTime"]["$gte"] = MatchDateStart
	query["MatchDateTime"]["$lte"] = MatchDateEnd
	local fullquery 	= 	{ query = query }
	
	local cursor_id,r,t = 	col_match:query( fullquery,{},0,1000,{} )
	local match_cal		=	50
	for index, item in pairs(r) do
		item["_id"] = nil
		
		ngx.say('XSMatchID : ' .. item['XSMatchID'])
		ngx.say(' | KPMatchID : ' .. item['id'])
		ngx.say(' => ' .. item['Team1'] .. '  <font color="#990000">' .. item['Team1FTScore'] .. ' - ' .. item['Team2FTScore'] .. '</font>  ' .. item['Team2'])
		ngx.say('<br>Match Date : ' .. item['MatchDateTime'])
		ngx.say('<br>Match Status : ' .. item['MatchStatus'])

		-- Save to Memcache
		if memc == nil then
			memc = get_memcache()
		end
		memc:set("Football2016-MatchInfo-" .. item['id'], cjson.encode(item),3600)
	end
	ngx.say( 'AutoMode = -1' )
	ngx.say( '<hr>' )
	--[[ Local[AutoMode = -1] -> Cache ]]--
	local query 		= 	{ AutoMode = -1 }
	local fullquery 	= 	{ query = query }
	
	local cursor_id,r,t = 	col_match:query( fullquery,{},0,1000,{} )
	local match_cal		=	50
	for index, item in pairs(r) do
		item["_id"] = nil
		
		ngx.say('XSMatchID : ' .. item['XSMatchID'])
		ngx.say(' | KPMatchID : ' .. item['id'])
		ngx.say(' => ' .. item['Team1'] .. '  <font color="#990000">' .. item['Team1FTScore'] .. ' - ' .. item['Team2FTScore'] .. '</font>  ' .. item['Team2'])
		ngx.say('<br>Match Date : ' .. item['MatchDateTime'])
		ngx.say('<br>Match Status : ' .. item['MatchStatus'])
		
		if item['MatchStatus'] == "1 HF" and type(item['StartDateTime']) ~= "nil" then
			local tmptime = os.time() - strtotime(item['StartDateTime'])
			item['Minute'] = math.floor(tmptime/60)
			ngx.say('  (' .. item['Minute'] .. '") ')
			local query_set = {}
			query_set["$set"] = { Minute = item['Minute']}
			local n, err = col_match:update({ id = item['id'] },query_set)
		elseif item['MatchStatus'] == "2 HF" and type(item['StartDateTime']) ~= "nil" then
			local tmptime = os.time() - strtotime(item['StartDateTime'])
			item['Minute'] = (math.floor(tmptime/60)+45)
			ngx.say('  (' .. item['Minute'] .. '") ')
			local query_set = {}
			query_set["$set"] = { Minute = item['Minute']}
			local n, err = col_match:update({ id = item['id'] },query_set)
		elseif item['MatchStatus'] == "E/T" and type(item['StartDateTime']) ~= "nil" then
			local tmptime = os.time() - strtotime(item['StartDateTime'])
			item['Minute'] = (math.floor(tmptime/60)+90)
			ngx.say('  (' .. item['Minute'] .. '") ')
			local query_set = {}
			query_set["$set"] = { Minute = item['Minute']}
			local n, err = col_match:update({ id = item['id'] },query_set)
		end

		-- Save to Memcache
		if memc == nil then
			memc = get_memcache()
		end
		memc:set("Football2016-MatchInfo-" .. item['id'], cjson.encode(item),3600)
	end
	ngx.say( '<hr>' )
	return
elseif data_arr[2] == "caculate_game" then
	ngx.header["Content-Type"] = "text/html; charset=utf-8"
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local col_match 	= 	db:get_col("football_match")
	local col_game 		= 	db:get_col("football_game")
	
	if type(ngx.var['arg_date']) ~= "nil" then
		tmpDate = ngx.var['arg_date']
		tmpYear,tmpMonth,tmpDay = tmpDate:match("(%d+)%-(%d+)%-(%d+)")
		tmpDateTime = os.time{year=tmpYear,month=tmpMonth,day=tmpDay}
		
		MatchDateStart = tmpDate .. " 06:00:00"
		MatchDateEnd = os.date("%Y-%m-%d 05:59:59",tmpDateTime+(60*60*24))
	else
		MatchDateStart = os.date("%Y-%m-%d %H:%M:%S",os.time()-(60*60*6))
		MatchDateEnd = os.date("%Y-%m-%d %H:%M:%S",os.time()-(60*60*2))
	end
	
	ngx.say( 'CHECK BETWEEN => ' .. MatchDateStart .. ' => ' .. MatchDateEnd )
	ngx.say( '<hr>' )
	
	local query 		= 	{ MatchDateTime = {} , MatchStatus = {} , Odds = {} }
	query["MatchDateTime"]["$gte"] = MatchDateStart
	query["MatchDateTime"]["$lte"] = MatchDateEnd
	
	query["MatchStatus"]["$in"] = {'Fin','Post','Abd','Canc'}
	
	query["Odds"]["$gte"]		= -1
	
	local fullquery 	= 	{ query = query }
	local cursor_id,r,t = 	col_match:query( fullquery,{},0,10000,{} )
	log_db("query","col_match")
	for index, item in pairs(r) do
	
		local tmp_match_datagame = {}
		local gameTypeBet = 0
		local isFull = false
				
		if item["MatchStatus"] == "Fin" then
			if item["Odds"] == -1 then
				gameTypeBet = 3
			else
				if type(item["TeamOdds"]) ~= nil then
					if item["TeamOdds"] == 1 then
						tmp_match_datagame["Team2FTScore"] = tonumber(item["Team2FTScore"]) + tonumber(item["Odds"])
						tmp_match_datagame["Team1FTScore"] = tonumber(item["Team1FTScore"])
						tmp_match_datagame["TeamOdds"] = 1
					elseif item["TeamOdds"] == 2 then
						tmp_match_datagame["Team1FTScore"] = tonumber(item["Team1FTScore"]) + tonumber(item["Odds"])
						tmp_match_datagame["Team2FTScore"] = tonumber(item["Team2FTScore"])
						tmp_match_datagame["TeamOdds"] = 2
					else
						tmp_match_datagame["Team1FTScore"] = tonumber(item["Team1FTScore"])
						tmp_match_datagame["Team2FTScore"] = tonumber(item["Team2FTScore"])
					end
				end
			
				if tmp_match_datagame["Team1FTScore"] > tmp_match_datagame["Team2FTScore"] then
					gameTypeBet = 1
					if math.abs(tmp_match_datagame["Team1FTScore"]-tmp_match_datagame["Team2FTScore"]) > 0.25 then
						isFull = true
					end
				elseif tmp_match_datagame["Team1FTScore"] < tmp_match_datagame["Team2FTScore"] then
					gameTypeBet = 2
					if math.abs(tmp_match_datagame["Team1FTScore"]-tmp_match_datagame["Team2FTScore"]) > 0.25 then
						isFull = true
					end
				else
					gameTypeBet = 3
				end
			end
		elseif item["MatchStatus"] == "Post" or item["MatchStatus"] == "Abd" or item["MatchStatus"] == "Canc" then
			gameTypeBet = 3
		end
		tmp_match_datagame["GameTypeBet"] 	= 	gameTypeBet
		tmp_match_datagame["isFull"] 		= 	isFull
		tmp_match_datagame["Odds"]			=	item["Odds"]
		tmp_match_datagame["id"]			=	item["id"]
		tmp_match_datagame["XSMatchID"]		=	item["XSMatchID"]
		tmp_match_datagame["TeamOdds"]		= 	item["TeamOdds"]

		if gameTypeBet > 0 then
			local query_game 									= 	{ listGame = {} }
			query_game["listGame"]["$elemMatch"] 				=	{}
			query_game["listGame"]["$elemMatch"]["match_id"] 	= 	item["id"]
			query_game["listGame"]["$elemMatch"]["status"] 		= 	1
			query_game["status"]								=	{}
			query_game["status"]["$gte"] 						= 	1
			
			--ngx.say(cjson.encode(query_game))
			
			local fullquery_game 					= 	{ query = query_game }
			local cursor_id_game,r_game,t_game 		= 	col_game:query( fullquery_game,{},0,10000,{} )
			log_db("query","col_game")
			for index_game, item_game in pairs(r_game) do
				ngx.say(item_game["id"] .. '<br>')
				calculate_game(item_game["id"],item["id"],gameTypeBet,isFull)
			end
		end

		set_cache_redis("MatchGameInfo-XScoreID-" .. item["XSMatchID"],60*60*24,cjson.encode(tmp_match_datagame))

		---------------------- UPDATE MATCH MEMCACHED --------------------------
		
		ngx.say('XSMatchID : ' .. item['XSMatchID'] .. "| KPMatchID : " .. item['id'])
		ngx.say(' => ' .. item['Team1'] .. '  <font color="#990000">' .. item['Team1FTScore'] .. ' - ' .. item['Team2FTScore'] .. '</font>  ' .. item['Team2'])
		ngx.say('<br>Match Date : ' .. item['MatchDateTime'])
		ngx.say('<br>Match Status : ' .. item['MatchStatus'])
		ngx.say('  (' .. item['Minute'] .. '") ')
		ngx.say('<br>')
		ngx.say('<br>')

	end
	
	ngx.say( '<hr>' )
	
	return
elseif data_arr[2] == "test_xscore" then
	local match_id = ngx.var['arg_match_id']
	local LastScorers = get_scorer(match_id)
	ngx.say(cjson.encode(LastScorers))
elseif data_arr[2] == "update_scorer" then

	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end

	local col_match 	= 	db:get_col("football_match")
	local match_id = tonumber(ngx.var['arg_match_id'])
	local match_data = get_match_by_id(match_id)
	local query_set = {}
	
	match_data["LastScorers"] = get_scorer(match_data["XSMatchID"])
	query_set["$set"] = match_data
	local n, err = col_match:update({ XSMatchID = match_data['XSMatchID'], AutoMode = 1 },query_set)
	log_db("update","col_match")
	if memc == nil then
		memc = get_memcache()
	end
	local ok, err = memc:delete('Football2014-MatchContents-' .. match_id)
elseif data_arr[2] == "remove_duplicate_bonus" and type(ngx.var['arg_date']) ~= "nil" then
	local tmpDate = ngx.var['arg_date']
	local tmpYear,tmpMonth,tmpDay = tmpDate:match("(%d+)%-(%d+)%-(%d+)")
	local start_day = 1
	local start_month = 8
	
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	
	local col_reward 	= 	db:get_col("football_reward")

	local query 		= 	{}
	query["key"]		=	tmpDate
	
	local fullquery 	= 	{ query = query }
	fullquery["orderby"] = 	{}
	fullquery["orderby"]["user_id"] = -1
	
	local tmpID = -1
	
	local cursor_id,r,t = 	col_reward:query( fullquery,{},0,10000,{} )
	log_db("update","col_reward")
	for index, item in pairs(r) do
		if tmpID == item["user_id"]  then
		
			ngx.say(item["user_id"] .. ":" .. item["datetime"])
			
			local query_set 	= 	{}
			query_set["$inc"] 	= 	{ total_point = tonumber(0-item["value"]) }
			
			-- YEARLY
			local col_season 	= 	db:get_col("football_ranking_season")
			local n, err 		= 	col_season:update({ season = tostring(tmpYear), id = tonumber(item["user_id"]) },query_set,1)
			log_db("update","col_season")
			
			-- Monthly
			local col_month 	= 	db:get_col("football_ranking_month")
			local n, err 		= 	col_month:update({ month = tmpYear .. "-" .. tmpMonth, id = tonumber(item["user_id"]) },query_set,1)
			log_db("update","col_month")
			
			-- Daily
			local col_day 		= 	db:get_col("football_ranking_day")
			local query_con 	=	{ id = tonumber(item["user_id"]) }
			query_con["date"]	=	tmpDate
			local n, err 		= 	col_day:update(query_con,query_set,1)
			log_db("update","col_day")
			
			-- ALL Time
			local col_alltime 	= 	db:get_col("football_ranking_all")
			local n, err 		= 	col_alltime:update({ id = tonumber(item["user_id"]) },query_set,1)
			log_db("update","col_alltime")

			-- CURRENT Point
			if os.date("%Y-%m",tmpTime) == tostring(tmpYear .. "-" .. tmpMonth) then
				local col_member 	= 	db:get_col("football_member")
				query_set 			= 	{}
				query_set["$inc"] 	= 	{ point = tonumber(0-item["value"]) }
				local n, err 		=	col_member:update({ id = tonumber(item["user_id"]) },query_set,1)
				log_db("update","col_member")
			end
			local n, err = col_reward:delete({ id = item["id"]},1)
			log_db("delete","col_reward")
			
		end
		tmpID = item["user_id"]
	end
	ngx.say("--------------------------------")
elseif data_arr[2] == "load_live_match_to_memcache" then
	if type(ngx.var['arg_mid']) ~= "nil" then
		ngx.header["Content-Type"] = "application/json; charset=utf-8"
		ngx.header["Access-Control-Allow-Origin"] = "http://football.kapook.com"
		
		local callback_str = nil
		local arr_mid = explode(",",ngx.var['arg_mid'])
		local result = {}
		
		if type(ngx.var['arg_callback']) ~= "nil" then
			callback_str = ngx.var['arg_callback']
		end

		for index, item in pairs(arr_mid) do
			if tonumber(item) ~= nil then
				if memc == nil then
					memc = get_memcache()
				end
				local MatchInfo, err = memc:get("Football2016-MatchInfo-" .. item)
					
				if type(ngx.var['arg_VsStyle']) ~= "nil" and MatchInfo ~= nil then
					--ngx.say(MatchInfo)
					MatchInfo = cjson.decode(MatchInfo)
					if(ngx.var['arg_VsStyle'] == "2") then
						local scorer_str = ""
						for key,str in pairs(MatchInfo['LastScorers']) do
							if scorer_str == "" then
								scorer_str = str
							else
								scorer_str = scorer_str .. "|" .. str
							end
						end
						result[item]	= 	MatchInfo['MatchStatus'] .. ':' .. MatchInfo['FTScore'] .. ':' .. MatchInfo['ETScore'] .. ':' .. MatchInfo['PNScore'] .. ':' .. MatchInfo['Minute'] .. ':' .. scorer_str
					else
						result[item]	= 	MatchInfo['MatchStatus'] .. ':' .. MatchInfo['FTScore'] .. ':' .. MatchInfo['Minute'] .. ':' .. MatchInfo['ETScore'] .. ':' .. MatchInfo['PNScore']
					end
				elseif MatchInfo ~= nil then
					result[item]	=	cjson.decode(MatchInfo)
				else
					result[item]	=	false
				end
			end
		end

		ngx.say(json_say(result,callback_str))
	else
		ngx.header["Content-Type"] = "text/html; charset=utf-8"
		if conn == nil then
			conn = get_conn_mongo()
		end
		if db == nil then
			db = conn:new_db_handle ( "football" )
		end
		local col_match 	= 	db:get_col("football_match")
	
		if type(ngx.var['arg_date']) ~= "nil" then
			tmpDate = ngx.var['arg_date']
			tmpYear,tmpMonth,tmpDay = tmpDate:match("(%d+)%-(%d+)%-(%d+)")
			tmpDateTime = os.time{year=tmpYear,month=tmpMonth,day=tmpDay}
			
			MatchDateStart = tmpDate .. " 06:00:00"
			MatchDateEnd = os.date("%Y-%m-%d 05:59:59",tmpDateTime+(60*60*24))
		else
			MatchDateStart = os.date("%Y-%m-%d %H:%M:%S",os.time()-(60*60*4))
			MatchDateEnd = os.date("%Y-%m-%d %H:%M:%S",os.time()+(60*30))
		end
		
		if type(ngx.var['arg_show']) ~= "nil" then
			ngx.say( 'CHECK BETWEEN => ' .. MatchDateStart .. ' => ' .. MatchDateEnd )
			ngx.say( '<hr>' )
		end
		
		local query 	= 	{ MatchDateTime = {} }
		query["MatchDateTime"]["$gte"] = MatchDateStart
		query["MatchDateTime"]["$lte"] = MatchDateEnd
		local fullquery 	= 	{ query = query }
		local cursor_id,r,t = 	col_match:query( fullquery,{},0,10000,{} )
		log_db("query","col_match")
		for index, item in pairs(r) do
			-- unset "_id"
			item["_id"] = nil
			
			-- Setting Time
			item["CreateDateMongo"] = { sec = math.floor(item["CreateDateMongo"]/1000) , usec = math.fmod(item["CreateDateMongo"],1000)*1000 }
			item["MatchDateTimeMongo"] = { sec = math.floor(item["MatchDateTimeMongo"]/1000) , usec = math.fmod(item["MatchDateTimeMongo"],1000)*1000 }
			item["LastUpdateMongo"] = { sec = math.floor(item["LastUpdateMongo"]/1000) , usec = math.fmod(item["LastUpdateMongo"],1000)*1000 }

			-- Record update time
			item["updatetime_cache"] = os.date("%Y-%m-%d %H:%M:%S")
			
			-- Save to Memcache
			if memc == nil then
				memc = get_memcache()
			end
			memc:set("Football2016-MatchInfo-" .. item["id"], cjson.encode(item),3600)
			if type(ngx.var['arg_show']) ~= "nil" then
				ngx.say(item['id'] .. ':' .. item['MatchStatus'] .. ':' .. item['Minute'] .. ':' .. item['KPLeagueID'])
				ngx.say('<br>')
			end
		end
		ngx.say("--------------------------------")
	end
elseif data_arr[2] == "clear_reward_2015" then
	ngx.header["Content-Type"] = "text/html; charset=utf-8"
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	local col_reward 	= 	db:get_col("football_reward")
	local query = { datetime = {} }
	query["datetime"]["$lte"] = "2016-01-01 06:00:00" 
	local n, err = col_reward:delete(query, 0, 0)
	ngx.say("Finish")
elseif data_arr[2] == "clear_notification_2015" then
	ngx.header["Content-Type"] = "text/html; charset=utf-8"
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	local col_noti 	= 	db:get_col("football_log_notify")
	local query = { time_stamp = {} }
	query["time_stamp"]["$lte"] = "2016-01-01 06:00:00" 
	local n, err = col_noti:delete(query, 0, 0)
	ngx.say("Finish")
elseif data_arr[2] == "clear_game" then
	ngx.header["Content-Type"] = "text/html; charset=utf-8"
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	local col_game 	= 	db:get_col("football_game")
	local query = { date_play = {} }
	query["date_play"]["$lt"] = "2015-08-01" 
	local n, err = col_game:delete(query, 0, 0)
	ngx.say("Finish")
elseif data_arr[2] == "get_matchcache" then
	if red == nil then
		red = get_redis()
	end
	ngx.say(red:get("match_" .. data_arr[3]))
elseif data_arr[2] == "caculate_game_test" then
	ngx.header["Content-Type"] = "text/html; charset=utf-8"
	if conn == nil then
		conn = get_conn_mongo()
	end
	if db == nil then
		db = conn:new_db_handle ( "football" )
	end
	local col_match 	= 	db:get_col("football_match")
	local col_game 		= 	db:get_col("football_game")
	
	if type(ngx.var['arg_date']) ~= "nil" then
		tmpDate = ngx.var['arg_date']
		tmpYear,tmpMonth,tmpDay = tmpDate:match("(%d+)%-(%d+)%-(%d+)")
		tmpDateTime = os.time{year=tmpYear,month=tmpMonth,day=tmpDay}
		
		MatchDateStart = tmpDate .. " 06:00:00"
		MatchDateEnd = os.date("%Y-%m-%d 05:59:59",tmpDateTime+(60*60*24))
	elseif type(ngx.var['arg_startdate']) ~= "nil" and type(ngx.var['arg_enddate']) ~= "nil" then
		tmpDate = ngx.var['arg_enddate']
		tmpYear,tmpMonth,tmpDay = tmpDate:match("(%d+)%-(%d+)%-(%d+)")
		tmpDateTime = os.time{year=tmpYear,month=tmpMonth,day=tmpDay}
		
		MatchDateStart = ngx.var['arg_startdate'] .. " 06:00:00"
		MatchDateEnd = os.date("%Y-%m-%d 05:59:59",tmpDateTime+(60*60*24))
	end
	
	local match_count = 0
	local game_count = 0
	
	ngx.say( 'CHECK BETWEEN => ' .. MatchDateStart .. ' => ' .. MatchDateEnd )
	ngx.say( '<hr>' )
	
	local query 		= 	{ MatchDateTime = {} , MatchStatus = {} , Odds = {} }
	query["MatchDateTime"]["$gte"] = MatchDateStart
	query["MatchDateTime"]["$lte"] = MatchDateEnd
	
	query["MatchStatus"]["$in"] = {'Fin','Post','Abd','Canc'}
	
	query["Odds"]["$gte"]		= -1
	
	local fullquery 	= 	{ query = query }
	local cursor_id,r,t = 	col_match:query( fullquery,{},0,10000,{} )
	log_db("query","col_match")
	for index, item in pairs(r) do
	
		local tmp_match_datagame = {}
		local gameTypeBet = 0
		local isFull = false
				
		if item["MatchStatus"] == "Fin" then
			if item["Odds"] == -1 then
				gameTypeBet = 3
			else
				if type(item["TeamOdds"]) ~= nil then
					if item["TeamOdds"] == 1 then
						tmp_match_datagame["Team2FTScore"] = tonumber(item["Team2FTScore"]) + tonumber(item["Odds"])
						tmp_match_datagame["Team1FTScore"] = tonumber(item["Team1FTScore"])
						tmp_match_datagame["TeamOdds"] = 1
					elseif item["TeamOdds"] == 2 then
						tmp_match_datagame["Team1FTScore"] = tonumber(item["Team1FTScore"]) + tonumber(item["Odds"])
						tmp_match_datagame["Team2FTScore"] = tonumber(item["Team2FTScore"])
						tmp_match_datagame["TeamOdds"] = 2
					else
						tmp_match_datagame["Team1FTScore"] = tonumber(item["Team1FTScore"])
						tmp_match_datagame["Team2FTScore"] = tonumber(item["Team2FTScore"])
					end
				end
			
				if tmp_match_datagame["Team1FTScore"] > tmp_match_datagame["Team2FTScore"] then
					gameTypeBet = 1
					if math.abs(tmp_match_datagame["Team1FTScore"]-tmp_match_datagame["Team2FTScore"]) > 0.25 then
						isFull = true
					end
				elseif tmp_match_datagame["Team1FTScore"] < tmp_match_datagame["Team2FTScore"] then
					gameTypeBet = 2
					if math.abs(tmp_match_datagame["Team1FTScore"]-tmp_match_datagame["Team2FTScore"]) > 0.25 then
						isFull = true
					end
				else
					gameTypeBet = 3
				end
			end
		elseif item["MatchStatus"] == "Post" or item["MatchStatus"] == "Abd" or item["MatchStatus"] == "Canc" then
			gameTypeBet = 3
		end
		tmp_match_datagame["GameTypeBet"] 	= 	gameTypeBet
		tmp_match_datagame["isFull"] 		= 	isFull
		tmp_match_datagame["Odds"]			=	item["Odds"]
		tmp_match_datagame["id"]			=	item["id"]
		tmp_match_datagame["XSMatchID"]		=	item["XSMatchID"]
		tmp_match_datagame["TeamOdds"]		= 	item["TeamOdds"]

		if gameTypeBet > 0 then
			local query_game 									= 	{ listGame = {} }
			query_game["listGame"]["$elemMatch"] 				=	{}
			query_game["listGame"]["$elemMatch"]["match_id"] 	= 	item["id"]
			
			local fullquery_game 					= 	{ query = query_game }
			local cursor_id_game,r_game,t_game 		= 	col_game:query( fullquery_game,{},0,10000,{} )
			log_db("query","col_game")
			for index_game, item_game in pairs(r_game) do
			--	ngx.say(item_game["id"] .. '<br>')
				calculate_game_test(item_game["id"],item["id"],gameTypeBet,isFull)
				game_count = game_count + 1
				log_db("games",game_count)
			end
		end

		set_cache_redis("MatchGameInfo-XScoreID-" .. item["XSMatchID"],60*60*24,cjson.encode(tmp_match_datagame))

		---------------------- UPDATE MATCH MEMCACHED --------------------------
		--[[
		ngx.say('XSMatchID : ' .. item['XSMatchID'] .. "| KPMatchID : " .. item['id'])
		ngx.say(' => ' .. item['Team1'] .. '  <font color="#990000">' .. item['Team1FTScore'] .. ' - ' .. item['Team2FTScore'] .. '</font>  ' .. item['Team2'])
		ngx.say('<br>Match Date : ' .. item['MatchDateTime'])
		ngx.say('<br>Match Status : ' .. item['MatchStatus'])
		ngx.say('  (' .. item['Minute'] .. '") ')
		ngx.say('<br>')
		ngx.say('<br>')
		]]--
		match_count = match_count+1
		log_db("match",match_count)
		
		ngx.say( "Match: " .. match_count .. " |Games: " .. game_count .. "<br/>" )

	end
	
	ngx.say( '<hr>' )
	
	return
elseif data_arr[2] == "csv" then
	ngx.header["Content-Type"] = "text/csv; charset=utf-8"
	if data_arr[3] == "team" then
		if conn == nil then
			conn = get_conn_mongo()
		end
		if db == nil then
			db = conn:new_db_handle ( "football" )
		end
		local col_team	= 	db:get_col("football_team")
		
		local fullquery 	= 	{ }
		local cursor_id,r,t = 	col_team:query( fullquery,{},0,7000,{} )
		for index, item in pairs(r) do
			ngx.say( item['id'] .. ',"' .. item['NameEN'] ..  '","' .. item['NameTH'] .. '","' .. item['NameTHShort'] .. '"')
		end
	elseif data_arr[3] == "match" then
		if conn == nil then
			conn = get_conn_mongo()
		end
		if db == nil then
			db = conn:new_db_handle ( "football" )
		end
		local col_match 	= 	db:get_col("football_match")
		
		if type(ngx.var['arg_date']) ~= "nil" then
			tmpDate = ngx.var['arg_date']
			tmpYear,tmpMonth,tmpDay = tmpDate:match("(%d+)%-(%d+)%-(%d+)")
			tmpDateTime = os.time{year=tmpYear,month=tmpMonth,day=tmpDay}
			
			MatchDateStart = tmpDate .. " 06:00:00"
			MatchDateEnd = os.date("%Y-%m-%d 05:59:59",tmpDateTime+(60*60*24))
		else
			MatchDateStart = os.date("%Y-%m-%d %H:%M:%S",os.time()-(60*60*6))
			MatchDateEnd = os.date("%Y-%m-%d %H:%M:%S",os.time()-(60*60*2))
		end
		
		local query 		= 	{ MatchDateTime = {} , Odds = {} }
		query["MatchDateTime"]["$gte"] = MatchDateStart
		query["MatchDateTime"]["$lte"] = MatchDateEnd

		query["Odds"]["$gt"]		= -1
		
		local fullquery 	= 	{ query = query }
		local cursor_id,r,t = 	col_match:query( fullquery,{},0,10000,{} )

		for index, item in pairs(r) do
			ngx.say( item['id'] .. ',' .. item['Team1KPID'] ..  ',' .. item['Team2KPID'] .. ',"' .. item['MatchDateTime'] .. '"')
		end
	elseif data_arr[3] == "game" then
		if conn == nil then
			conn = get_conn_mongo()
		end
		if db == nil then
			db = conn:new_db_handle ( "football" )
		end
		local col_game 	= 	db:get_col("football_game")
		
		if type(ngx.var['arg_date']) ~= "nil" then
			tmpDate = ngx.var['arg_date']
		else
			local tmpHour = tonumber(os.date("%H",os.time()))
			if tmpHour>= 6 then
				tmpDate = os.date("%Y-%m-%d",os.time())
			else
				tmpDate = os.date("%Y-%m-%d",os.time()-(60*60*24))
			end
		end
		
		local query 		= 	{ date_play = tmpDate }
		local fullquery 	= 	{ query = query }
		local cursor_id,r,t = 	col_game:query( fullquery,{},0,10000,{} )

		for index, item in pairs(r) do
			for i,game in pairs(item['listGame']) do
				if type(game['isWDL']) ~= "nil" then
					ngx.say( item['id'] .. ',' .. game['match_id'] ..  ',' .. game['team_win'] .. ',"' .. item['timestamp'] .. '",' .. item['user_id'] .. ',' .. item['date_play'] .. ',' .. game['isWDL'] )
				else
					ngx.say( item['id'] .. ',' .. game['match_id'] ..  ',' .. game['team_win'] .. ',"' .. item['timestamp'] .. '",' .. item['user_id'] .. ',' .. item['date_play'])
				end
			end
		end
	end
elseif data_arr[2] == "swap" and type(data_arr[3]) ~= "nil" and type(ngx.var['arg_pong']) ~= "nil" then
	ngx.header["Content-Type"] = "text/html; charset=utf-8"
	local match_id = tonumber(data_arr[3])

	if match_id > 0 then
		ngx.say(match_id)
		-- DB Connect --
		if conn == nil then
			conn = get_conn_mongo()
		end
		if db == nil then
			db = conn:new_db_handle ( "football" )
		end
		
		-- Game --
		local col_game 										= 	db:get_col("football_game")
		local query_game 									= 	{ listGame = {} }
		query_game["listGame"]["$elemMatch"] 				=	{}
		query_game["listGame"]["$elemMatch"]["match_id"] 	= 	match_id
		local fullquery_game 								= 	{ query = query_game }
		local cursor_id_game,r_game,t_game 					= 	col_game:query( fullquery_game,{},0,10000,{} )
		for index_game, item_game in pairs(r_game) do
			ngx.say(item_game["id"] .. '<br>')
			for index, item in pairs(item_game["listGame"]) do
				if item["match_id"] == match_id then
					if item["team_win"] == 1 then
						item["team_win"] = 2
					elseif item["team_win"] == 2 then
						item["team_win"] = 1
					end
					item_game["listGame"][index] = item
				end
			end
			local query_set_game 	= 	{}
			query_set_game["$set"] 	= 	item_game
			local n, err = col_game:update({ id = tonumber(item_game["id"]) },query_set_game,1)
			log_db("update","calculate_game-game")
		end
		
		-- Match --
		local col_match = db:get_col("football_match")
		local match_data = get_match_by_id(match_id)
		local tmp_id = match_data["Team1KPID"]
		match_data["Team1KPID"] = match_data["Team2KPID"]
		match_data["Team2KPID"] = tmp_id
		
		local tmp_name = match_data["Team1"]
		match_data["Team1"] = match_data["Team2"]
		match_data["Team2"] = tmp_name
		
		local tmp_count = match_data["count_playside_team1"]
		match_data["count_playside_team1"] = match_data["count_playside_team2"]
		match_data["count_playside_team2"] = tmp_count
		
		if match_data["TeamOdds"] == 1 then
			match_data["TeamOdds"] = 2
		elseif match_data["TeamOdds"] == 2 then
			match_data["TeamOdds"] = 1
		end
		
		if match_data["TeamSelect"] == 1 then
			match_data["TeamSelect"] = 2
		elseif match_data["TeamSelect"] == 2 then
			match_data["TeamSelect"] = 1
		end
		
		local query_set_match = {}
		query_set_match["$set"] = match_data
		local n, err = col_match:update({ id = match_id },query_set_match)
		log_db("update","swap")
	end
	ngx.say("Finish")
elseif data_arr[2] == "test_test" then
	ngx.say("TEst55555")
else
	ngx.say("TEst1.0.1.2")
end