-- CJSON
local cjson = require "cjson"
-- MONGODB
local mongo = require "resty.mongol"
conn = mongo:new()
conn:set_timeout(1000)
ok, err = conn:connect("192.168.1.189",27017)
if not ok then
	ngx.say("connect failed1: "..err)
end
local db = conn:new_db_handle ( "football" )
-- MONGO BSON
local bson = require('resty.mongol.bson')
local get_utc_date = bson.get_utc_date
-- HTTP
local http = require "resty.http"
local httpc = http.new()
-- REDIS
local redis = require "resty.redis"
red = redis:new()
red:set_timeout(1000) -- 1 sec
ok, err = red:connect("192.168.1.78", 6379)
if not ok then
	ngx.say("connect failed: "..err)
end
red:select(9)
-- MEMCACHED
local memcached = require "resty.memcached"
local memc, err = memcached:new()
local ok, err = memc:connect("192.168.1.189", 11211)
if not ok then
	ngx.say("failed to connect: ", err)
	return
end

-- GET DATE --
if tonumber(os.date("%H")) < 6 then
	tmpTime = os.time()-(60*60*24)
else
	tmpTime = os.time()
end

function isint(n)
	return n==math.floor(n)
end

function explode(delimiter, text)
	local list = {};
	local pos = 1
	if string.find("", delimiter, 1) then
		-- We'll look at error handling later!
		error("delimiter matches empty string!")
	end
	--ngx.say(text)
	local i = 0
	while 1 do
		local first, last = string.find(text, delimiter, pos)
		--ngx.say (first, last)
		if first then
			list[i] = string.sub(text, pos, first-1)
			i = i +1
			pos = last+1
		else
			--table.insert(list, string.sub(text, pos))
			list[i] = string.sub(text, pos)
			break
		end
	end
	--ngx.say(list[1])
	return list
end

function implode(delimiter, array)
	local isFirst = true
	local return_str = ""
	for i in pairs(array) do
		if isFirst then
			isFirst = false
		else
			return_str = return_str .. ","
		end
		return_str = return_str .. array[i]
    end
	return return_str
end

function set_cache_redis(name_cache,time_cache,data_cache)
	ok, err = red:set(name_cache,data_cache)
	if not ok then
		ngx.say("failed to set : ", err)
		return false
	end
	ok, err = red:expire(name_cache,time_cache)
	if not ok then
		ngx.say("failed to set : ", err)
		return false
	end
	return true
end

function get_kapi_member(id)
	local res, err = httpc:request_uri("http://kapi.kapook.com/profile/member/userid/"..id, {
		method = "GET",
		headers = {
			["Content-Type"] = "application/x-www-form-urlencoded",
		}
	})		
	if res then
		return_data = cjson.decode(res.body)
		if not return_data["data"] then
			return_data = { error_description = "Not Found" , status = -1 }
		end
	else
		return_data = { error_description = err , status = -1 }
	end
	return return_data["data"]
end

function get_cookie(cookie_key)
	local list_str_cookie = explode(";",ngx.var.http_cookie)
	for index,item in pairs(list_str_cookie) do
		local attr	=	explode("=",item)
		attr[0] = string.gsub(attr[0], "%s+", "")
		if attr[0]==cookie_key then
			attr[1] = string.gsub(attr[1], "%\"+", "")
			return attr[1]
		end
	end
	return nil
end

function check_cookie_login()
	local hash = "kapook_sudyod"
	local uid = get_cookie("uid")
	local is_login = get_cookie("is_login")

	if uid and is_login then
		if ngx.md5(uid .. hash) == is_login then
			local expires = 172800
			ngx.header['Set-Cookie'] = "uid=" .. uid .. "; path=/; Expires=" .. ngx.cookie_time(ngx.time() + expires)
			ngx.header['Set-Cookie'] = "is_login=" .. is_login .. "; path=/; Expires=" .. ngx.cookie_time(ngx.time() + expires)
			return tonumber(uid)
        else
			return false
		end
	else
		return false
	end
end

function lastest_id(col_name,field_name)
	local query 		= 	{}
	local collection 	= 	db:get_col(col_name)
	local return_data	=	1
	
	local fullquery = {}
	fullquery["query"] = query
	fullquery["orderby"] = {}
	fullquery["orderby"][0] = {}
	fullquery["orderby"][0][field_name] = -1
						
	local cursor_id,r,t = collection:query( fullquery,{},0,1,{} )

	for index, item in pairs(r) do
		return item[field_name]+1
	end
	return return_data
end

function get_match(id,status,match_status)
	local query 		= 	{}
	local collection 	= 	db:get_col("football_match")

	local time_date_from 	=	os.date("%Y-%m-%d 06:00:00",tmpTime)
	local time_date_to		=	os.date("%Y-%m-%d 05:59:59",tmpTime+(60*60*24))
	
	local query 			= {}
	
	query["id"] 			= id
	query["Status"] 		= status
	query["MatchStatus"] 	= match_status
	
	query["MatchDateTime"] 	= {}
	query["MatchDateTime"]["$gte"] 	= time_date_from
	query["MatchDateTime"]["$lte"] 	= time_date_to
	
	return collection:find_one(query)
end

function update_match_playside(id,playside)
	local query 		= 	{}
	local collection 	= 	db:get_col("football_match")
	local query_set 	= 	{}
	query_set["$inc"] 	= 	{}
	query_set["$inc"]["count_playside_team" .. playside] = 1
	query_set["$inc"]["count_playside"] = 1
	local n, err = collection:update({ id = tonumber(id) },query_set,1)
end

function get_member_data(id)
	local query 		= 	{}
	local collection 	= 	db:get_col("football_member")
	
	return collection:find_one({id = id})
end

function json_say(data,callback)
	if type(callback) == "string" then
		return callback .. "(" .. cjson.encode(data) .. ")"
	else
		return cjson.encode(data)
	end
end

local data_arr	=	explode('/',ngx.var.uri)

if data_arr[2] == "test" then
	ngx.header["Content-Type"] = "application/json; charset=utf-8"
	--[[local res, flags, err = memc:get("Football2014-ListGameID-" .. data_arr[3])
	local data = {data = res}
	if err then
		ngx.say("failed to get dog: ", err)
		return
	end
	ngx.say(cjson.encode(data))]]--
	--ngx.say(ngx.unescape_uri("%E0%B8%99%E0%B8%9E%E0%B8%9E%E0%B8%A3/test/some%20string?foo=bar"))
	--return 
elseif data_arr[2] == "insert" then
	ngx.header["Content-Type"] = "application/json; charset=utf-8"
	local callback_str = nil
	if type(ngx.var['arg_callback']) ~= "nil" then
		callback_str = ngx.var['arg_callback']
	end

	local present_single = 0
	local present_multiple = 0
	local used_point = 0
	
	local quota_playside_single		=	3;
	local quota_playside_multi		=	1;
	local qouta_playside_multi_min	=	2;
	local qouta_playside_multi_max	=	6;
	
	uid = check_cookie_login()
	
	if uid == false then
		ngx.say(json_say({
			code_id = 400,
			message = "Authentication is not success."
		},callback_str))
		return
	end
	
	data_member = get_member_data(uid)
	if data_member == nil then
		ngx.say(json_say({
			code_id = 2,
			message = "Point has not enough."
		},callback_str))
		return
	end
	
	ngx.req.read_body()
	local args, err = ngx.req.get_uri_args()
	if not args then
		ngx.say(json_say({
			code_id = 0,
			message = "failed to get post args: ", err
		},callback_str))
		return
	end
	
	local listMatch = {}
	local listGame = {}
	local size_match = 0
	
	for key, val in pairs(args) do
		if string.find(key,"team_win%[") then
			
			local s,e = string.find(key,"team_win%[")
			key = string.sub (key, e+1)
			
			local s,e = string.find(key,"%]")
			key = string.sub (key, 0 , s-1)
			
			if tonumber(val) ~= 1 and tonumber(val) ~= 2 then
				ngx.say(json_say({
					code_id =	6,
					message =	"number team select is wrong.",
					val = tonumber(val)
				},callback_str))
				return
			end
			
			listMatch[tonumber(key)] = tonumber(val)
			size_match = size_match + 1
			
		elseif key == "isSingle" then
			if tonumber(val) == 1 then
				isSingle = true
			else
				isSingle = false
			end
		elseif key == "point" then
			point = tonumber(val)
			if point < 0 then
				ngx.say(json_say({
					code_id =	1,
					message =	"point must over 0."
				},callback_str))
				return
			end
		end
	end

	if type(isSingle) == "nil" then
		isSingle = false
	end

	if type(point) ~= "number" then
		ngx.say(json_say({
			code_id =	1,
			message =	"point must Number."
		},callback_str))
		return
	end
	
	if isint(point) == false then
		ngx.say(json_say({
			code_id =	1,
			message =	"point must Number."
		},callback_str))
		return
	end

	if point <= 0 then
		ngx.say(json_say({
			code_id =	1,
			message =	"point must over 0."
		},callback_str))
		return
	end

	if size_match ~= 1 and isSingle then
		ngx.say(json_say({
			code_id =	3,
			message =	"Input is not Single,but you want single."
		},callback_str))
		return
	end
	
	if size_match < qouta_playside_multi_min and isSingle==false then
		ngx.say(json_say({
			code_id =	4,
			message =	"Input less minimum quota."
		},callback_str))
		return
	end
	
	if size_match > qouta_playside_multi_max and isSingle==false then
		ngx.say(json_say({
			code_id =	12,
			message =	"Input is over qouta for Multi."
		},callback_str))
		return
	end
	
	local collection 	= 	db:get_col("football_game")
	local query 		= 	{ date_play = os.date("%Y-%m-%d",tmpTime),user_id = uid }
	local fullquery 	= 	{ query = query }				
	local cursor_id,r,t = 	collection:query( fullquery,{},0,10,{} )
	for index, item in pairs(r) do
		if item["match_id"] < 0 then
			present_multiple 	= 	present_multiple+1
		else
			present_single 		= 	present_single+1
		end
		if item["status"] == 1 or item["status"] == 2 then
			used_point = used_point + item["point"]
		end
	end
	
	if (data_member["point"] - used_point) < point  then
		ngx.say(json_say({
			code_id = 2,
			message = "Point has not enough."
		},callback_str))
		return
	end
	
	if present_multiple >= quota_playside_multi and isSingle == false then
		ngx.say(json_say({
			code_id = 11,
			message = "Quota of Multi-playside is full."
		},callback_str))
		return
	end
	
	if present_single >= quota_playside_single and isSingle == true then
		ngx.say(json_say({
			code_id = 9,
			message = "Quota of single-playside is full."
		},callback_str))
		return
	end

	local tmpMatchID
	local index = 0
	for match_id, sideplay in pairs(listMatch) do
		
		local tmp_match = get_match( match_id, 1, "Sched" )
		
		if tmp_match == nil then
			ngx.say(json_say({
				code_id =	7,
				message =	"Not Found a Match(Match No." .. match_id .. ")."
			},callback_str))
			return
		else
			listGame[index] = {
				match_id 				= 	tonumber(match_id),
				team_win 				= 	tonumber(sideplay),
				status 					=	1,
				team_win_nameTHShort	= 	memc:get("Football2014-Team-NameTHShort-" .. tmp_match["Team" .. sideplay .. "KPID"] )
			}
			index = index + 1
		end
		tmpMatchID = match_id
		tmp_match["_id"] = nil
		set_cache_redis("matchinfo_" .. tmp_match["id"],60*60*24,cjson.encode(tmp_match))
		set_cache_redis("matchinfo_XSMatchID-" .. tmp_match["XSMatchID"],60*60*24,cjson.encode(tmp_match))
	end
	
	if isSingle == false then
		tmpMatchID = -1
	end
	
	local ok, err = memc:delete("Football2014-game-playside-list-info-" .. uid .. "-" .. os.date("%Y-%m-%d",tmpTime))
	
	local game_id = lastest_id("football_game","id")
	local matchListInGame = {}
	local n, err = collection:insert({{
		id = game_id,
		date_play = os.date("%Y-%m-%d",tmpTime),
		listGame = listGame,
		user_id = uid,
		status = 1,
		point = point,
		point_return = 0,
		multiple = 1.0,
		game_type = 2,
		timestamp = os.date("%Y-%m-%d %H:%M:%S"),
		match_id = tmpMatchID
	}})
	
	for match_id, sideplay in pairs(listMatch) do
		tmp_data, err = red:get("match_" .. match_id)
		if type(tmp_data) ~= "string" then
			tmp_data = {}
		else
			tmp_data = cjson.decode(tmp_data)
		end
		table.insert(tmp_data,game_id)
		set_cache_redis("match_" .. match_id,60*60*24,cjson.encode(tmp_data))
		
		table.insert(matchListInGame,match_id)
		
		update_match_playside(match_id,sideplay)
		
	end
	
	set_cache_redis("game_" .. game_id,60*60*24,cjson.encode(matchListInGame))
	set_cache_redis("gameinfo_" .. game_id,60*60*24,cjson.encode({
		id = game_id,
		date_play = os.date("%Y-%m-%d",tmpTime),
		listGame = listGame,
		user_id = uid,
		status = 1,
		point = point,
		point_return = 0,
		multiple = 1.0,
		game_type = 2,
		timestamp = os.date("%Y-%m-%d %H:%M:%S"),
		match_id = tmpMatchID
	}))
	
	ngx.say(json_say({
		code_id =	200,
		message =	"Insert data successful.",
		id = game_id,
		uid = uid,
		point_remain = data_member["point"] - used_point - point,
		point = point,
		isSingle = isSingle,
		data = listGame
	},callback_str))
	local ok, err = memc:delete("Football2014-GameData-" .. uid)
	
	return
	
elseif data_arr[2] == "update_from_vpn" then
	ngx.header["Content-Type"] = "text/html; charset=utf-8"
	
	-- VPN Singapore
	conn_vpn = mongo:new()
	conn_vpn:set_timeout(10000)
	ok, err = conn_vpn:connect("107.170.52.129",27017)
	if not ok then
		ngx.say("connect failed1: "..err)
	end
	local db_vpn 	= 	conn_vpn:new_db_handle( "football_xscore_feed" )
	local col_vpn 	= 	db_vpn:get_col("football_match_xscore")
	-- Local Match
	local col_match 	= 	db:get_col("football_match")
	
	if type(ngx.var['arg_date']) ~= "nil" then
		tmpDate = ngx.var['arg_date']
		tmpYear,tmpMonth,tmpDay = tmpDate:match("(%d+)%-(%d+)%-(%d+)")
		tmpDateTime = os.time{year=tmpYear,month=tmpMonth,day=tmpDay}
		
		MatchDateStart = tmpDate .. " 06:00:00"
		MatchDateEnd = os.date("%Y-%m-%d 05:59:59",tmpDateTime+(60*60*24))
	else
		MatchDateStart = os.date("%Y-%m-%d %H:%M:%S",os.time()-(60*60*3))
		MatchDateEnd = os.date("%Y-%m-%d %H:%M:%S")
	end
	
	ngx.say( 'CHECK BETWEEN => ' .. MatchDateStart .. ' => ' .. MatchDateEnd )
	ngx.say( '<hr>' )
	
	local query 		= 	{ MatchDateTime = {} }
	query["MatchDateTime"]["$gte"] = MatchDateStart
	query["MatchDateTime"]["$lte"] = MatchDateEnd
	local fullquery 	= 	{ query = query }
	local cursor_id,r,t = 	col_vpn:query( fullquery,{},0,10000,{} )
	for index, item in pairs(r) do
		---------------------- UPDATE MATCH --------------------------
		local update_data = {
			SourceDate		=	item["SourceDate"],
			SourceTime		=	item["SourceTime"],
			MatchDateTime	=	item["MatchDateTime"],
			MatchStatus		=	item['MatchStatus'],
			Team1YC			=	tonumber(item["Team1YC"]),
			Team2YC			=	tonumber(item["Team2YC"]),
			Team1RC			=	tonumber(item["Team1RC"]),
			Team2RC			=	tonumber(item["Team2RC"]),
			HTScore			=	item["HTScore"],
			FTScore			=	item["FTScore"],
			ETScore			=	item["ETScore"],
			PNScore			=	item["PNScore"],
			Team1FTScore	=	tonumber(item["Team1FTScore"]),
			Team2FTScore	=	tonumber(item["Team2FTScore"]),
			Minute			=	item["Minute"],
			LastScorers		=	item["LastScorers"],
			LastUpdate		=	os.date("Y-m-d H:i:s"),
			LastUpdateMongo = 	get_utc_date(ngx.time()*1000)
		}
		
		local query_set = {}
		query_set["$set"] = update_data
		
		local n, err = col_match:update({ XSMatchID = item['XSMatchID'], AutoMode = 1 },query_set)
	
		ngx.say('XSMatchID : ' .. item['XSMatchID'])
		ngx.say(' => ' .. item['Team1'] .. '  <font color="#990000">' .. item['Team1FTScore'] .. ' - ' .. item['Team2FTScore'] .. '</font>  ' .. item['Team2'])
		ngx.say('<br>Match Date : ' .. item['MatchDateTime'])
		ngx.say('<br>Match Status : ' .. item['MatchStatus'])
		ngx.say('  (' .. item['Minute'] .. '") ')
		ngx.say('<br>')
		ngx.say('<br>')
		
		---------------------- UPDATE GAME --------------------------

	end
	return
	
end