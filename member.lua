-- CJSON
local cjson = require "cjson"
-- MONGODB
local mongo = require "resty.mongol"
conn = mongo:new()
conn:set_timeout(1000)
ok, err = conn:connect("192.168.1.222",27017)
--ok, err = conn:connect("192.168.1.189",27017)
if not ok then
	ngx.say("connect failed1: "..err)
end
local db = conn:new_db_handle ( "member" )
-- HTTP
local http = require "resty.http"
local httpc = http.new()
-- MONGO BSON
local bson = require('resty.mongol.bson')
local get_utc_date = bson.get_utc_date
--REDIS
local redis = require "resty.redis"
red = redis:new()
red:set_timeout(1000) -- 1 sec
ok, err = red:connect("192.168.1.78", 6379)
if not ok then
	ngx.say("connect failed: "..err)
end
red:select(9)

function new_mongo_id(_id_str)
	if type(_id_str) ~= 'string' then
		return '','Not String'
	end

	local c = string.len(_id_str)
	if c ~= 24 then
		return '','Invalid _id'
	end

	local obid = require ( "resty.mongol.object_id" )
	istr = ""
	for i = 0, 11 do
		c = "0x" .. string.sub(_id_str, i * 2 + 1, i * 2 + 2)
		istr = istr .. string.char(tonumber(c))
	end
	local ob = obid.new(istr)
	return ob,''
end

function explode(delimiter, text)
	local list = {};
	local pos = 1
	if string.find("", delimiter, 1) then
		-- We'll look at error handling later!
		error("delimiter matches empty string!")
	end
	--ngx.say(text)
	local i = 0
	while 1 do
		local first, last = string.find(text, delimiter, pos)
		--ngx.say (first, last)
		if first then
			list[i] = string.sub(text, pos, first-1)
			i = i +1
			pos = last+1
		else
			--table.insert(list, string.sub(text, pos))
			list[i] = string.sub(text, pos)
			break
		end
	end
	--ngx.say(list[1])
	return list
end

function implode(delimiter, array)
	local isFirst = true
	local return_str = ""
	for i in pairs(array) do
		if isFirst then
			isFirst = false
		else
			return_str = return_str .. ","
		end
		return_str = return_str .. array[i]
    end
	return return_str
end

function set_cache_redis(name_cache,time_cache,data_cache)
	ok, err = red:set(name_cache,data_cache)
	if not ok then
		ngx.say("failed to set : ", err)
		return false
	end
	ok, err = red:expire(name_cache,time_cache)
	if not ok then
		ngx.say("failed to set : ", err)
		return false
	end
	return true
end

function get_kapi_member(id)
	local res, err = httpc:request_uri("http://kapi.kapook.com/profile/member/userid/"..id, {
		method = "GET",
		headers = {
			["Content-Type"] = "application/x-www-form-urlencoded",
		}
	})		
	if res then
		return_data = cjson.decode(res.body)
		if not return_data["data"] then
			return_data = { error_description = "Not Found" , status = -1 }
		end
	else
		return_data = { error_description = err , status = -1 }
	end
	
	return return_data["data"]
	
end

ngx.header["Content-Type"] = "application/json; charset=utf-8"
local data_arr	= explode('/',ngx.var.uri)
local prefix_pos = 0
local time_center = red:ttl("time_center")
if time_center <= 0 then
	time_center = 900
	set_cache_redis("time_center",time_center,"1")
end

if type(data_arr[2+prefix_pos]) ~= "string" then
	data_arr[2+prefix_pos] = ""
end

if type(data_arr[3+prefix_pos]) ~= "string" then
	data_arr[3+prefix_pos] = ""
end

if data_arr[1+prefix_pos] == "topic" and data_arr[2+prefix_pos] == "user" and string.len(data_arr[3+prefix_pos]) > 0 then
	
	local namecache = ""
	local topic_id  = -1
	local not_id = -1
	
	if type(ngx.var['arg_size']) ~= "nil" then
		if tonumber(ngx.var['arg_size']) > 0 then
			size = tonumber(ngx.var['arg_size'])
		else
			size = 10
		end
	else
		size = 10
	end
	
	if tonumber(data_arr[3+prefix_pos]) == nil then
		returnData = {
			status_code = 400,
			description = "input is not numberic."
		}
	elseif tonumber(data_arr[3+prefix_pos]) <= 0 then
		returnData = {
			status_code = 200,
			total_rows = 0,
			total_pages = 0,
			paged = 1,
			rows = 0,
			data = {},
			limit = size,
			query = {}
		}
	else
		if type(ngx.var['arg_sortby']) ~= "nil" then
			if ngx.var['arg_sortby'] == "cr_date" then
				sort = "topic_id"
			elseif ngx.var['arg_sortby'] == "view" then
				sort = "views"
			elseif ngx.var['arg_sortby'] == "id" then
				sort = "topic_id"
			else
				sort = "topic_id"
			end
		else
			sort = "topic_id"
		end

		if type(ngx.var['arg_topicid']) ~= "nil" and sort == "topic_id" then
			topic_id = tonumber(ngx.var['arg_topicid'])
			namecache = "topic_user_" .. data_arr[3+prefix_pos] .. "_after-topic-" .. topic_id .. "_" .. size .. "_id"
			page = 1
		elseif type(ngx.var['arg_topicid']) ~= "nil" and sort == "views" then
			topic_id = tonumber(ngx.var['arg_topicid'])
			namecache = "topic_user_" .. data_arr[3+prefix_pos] .. "_after-topic-" .. topic_id .. "_" .. size .. "_views"
			
			col = db:get_col("topic")
			topic_data, err = red:get("topic_" .. topic_id)
			if not topic_data then
				ngx.say("failed to get: ", err)
				return
			end
			
			if topic_data == ngx.null then
				topic_data = col:find_one({topic_id = tonumber(topic_id)})
				if type(topic_data) ~= "nil" then
					topic_data["_id"] = nil
					topic_data["cr_date"] =  {
						sec = math.floor(topic_data["cr_date"] / 1000),
						usec = math.floor(math.fmod(topic_data["cr_date"],1000))*1000
					}
					if type(topic_data["picture"]) == "string" then 
						topic_data["picture"] = "http://memberapi.kapook.com/uploads/" .. topic_data["picture"]
					else
						topic_data["picture"] = "http://member.kapook.com/assets/images/bg-test.jpg"
					end
					set_cache_redis("topic_" .. topic_data["topic_id"],time_center,cjson.encode(topic_data))
				end
			else
				topic_data = cjson.decode(topic_data)
			end
			
			if type(topic_data) ~= "nil" then
				views = topic_data["views"]
			else
				views = 0
				topic_data = ngx.null
			end
			page = 1
		else
			if type(ngx.var['arg_page']) ~= "nil" then
				if tonumber(ngx.var['arg_page']) > 0 then
					page = tonumber(ngx.var['arg_page'])
				else
					page = 1
				end
			else
				page = 1
			end
			namecache = "topic_user_" .. data_arr[3+prefix_pos] .. "_" .. size .. "_" .. page .. "_" .. sort
		end
		
		if type(ngx.var['arg_notid']) ~= "nil" then
			not_id = tonumber(ngx.var['arg_notid'])
			namecache = namecache .. "_not-" .. ngx.var['arg_notid']
		end

		local isClear = false

		if type(ngx.var['arg_clear']) ~= "nil" then
			if ngx.var['arg_clear'] == "1" then
				isClear = true
			end
		end
		
		if isClear == false then
			returnData, err = red:get(namecache)
			
			if not returnData then
				ngx.say("failed to get: ", err)
				return
			end
		else
			returnData = ngx.null
		end
		
		if returnData == ngx.null then
			
			local query 		= 	{}
			local list_data		=	{}
			seek_pos = 0
				
			if type(topic_data) ~= "nil" then
				
				query["m_id"] = tonumber(data_arr[3+prefix_pos])
				query["views"] = views
						
				fullquery = {}
				fullquery["query"] = query
				fullquery["orderby"] = {}
				fullquery["orderby"][0] = {}
				fullquery["orderby"][0]["views"] = -1
				fullquery["orderby"][1] = {}
				fullquery["orderby"][1]["topic_id"] = -1
						
				cursor_id,r,t = col:query( fullquery,{},0,10000,{} )
				n = col:count(query)

				for index, item in pairs(r) do
					
					item["_id"] = nil
					item["cr_date"] =  {
						sec = math.floor(item["cr_date"] / 1000),
						usec = math.floor(math.fmod(item["cr_date"],1000))*1000
					}
					if type(item["picture"]) == "string" then 
						item["picture"] = "http://memberapi.kapook.com/uploads/" .. item["picture"]
					else
						item["picture"] = "http://member.kapook.com/assets/images/bg-test.jpg"
					end
					set_cache_redis("topic_" .. item["topic_id"],time_center,cjson.encode(item))
						
					if item["topic_id"] == tonumber(topic_id) then
						seek_pos = seek_pos + 1
						break
					end
					seek_pos = seek_pos + 1
				end
			end
			
			query = {}
			query["m_id"] = tonumber(data_arr[3+prefix_pos])
			
			if topic_id > 0 then
				if sort == "views" then
					query["views"] = {}
					query["views"]["$lte"] = views
				else
					query["topic_id"] = {}
					query["topic_id"]["$lt"] = topic_id
				end
			end
			
			if not_id > 0 then
				query["topic_id"] = {}
				query["topic_id"]["$ne"] = not_id
			end
			
			fullquery = {}
			fullquery["query"] = query
			fullquery["orderby"] = {}
			fullquery["orderby"][0] = {}
			fullquery["orderby"][0][sort] = -1
			if sort == "views" then
				fullquery["orderby"][1] = {}
				fullquery["orderby"][1]["topic_id"] = -1
			end
			
			col = db:get_col("topic")
			cursor_id,r,t = col:query( fullquery,{},((page-1)*size)+seek_pos,size,{} )
			n = col:count(query)

			returnData = {}
			for index, item in pairs(r) do
				item["_id"] = nil
				item["cr_date"] =  {
					sec = math.floor(item["cr_date"] / 1000),
					usec = math.floor(math.fmod(item["cr_date"],1000))*1000
				}
				if type(item["picture"]) == "string" then 
					item["picture"] = "http://memberapi.kapook.com/uploads/" .. item["picture"]
				else
					item["picture"] = "http://member.kapook.com/assets/images/bg-test.jpg"
				end
				set_cache_redis("topic_" .. item["topic_id"],time_center,cjson.encode(item))
				
				list_data[index] = item
			end
			
			returnData = {
				status_code = 200,
				total_rows = n,
				total_pages = math.ceil(n/size),
				paged = page,
				rows = n,
				data = list_data,
				limit = size,
				query = fullquery
			}
			set_cache_redis(namecache,time_center,cjson.encode(returnData))
			
			data_member = get_kapi_member(tonumber(data_arr[3+prefix_pos]))
			set_cache_redis("member_" .. tonumber(data_arr[3+prefix_pos]),time_center,cjson.encode(data_member))
		else
			returnData = cjson.decode(returnData)
			returnData["time_cache"] = red:ttl(namecache)
			returnData["is_cache"] = true
			returnData["name_cache"] = namecache
			
			data_member, err = red:get("member_" .. tonumber(data_arr[3+prefix_pos]))
			if not data_member then
				ngx.say("failed to get: ", err)
				return
			end
			if data_member == ngx.null then
				data_member = get_kapi_member(tonumber(data_arr[3+prefix_pos]))
				set_cache_redis("member_" .. tonumber(data_arr[3+prefix_pos]),time_center,cjson.encode(data_member))
			else
				data_member = cjson.decode(data_member)
			end
			
		end
		
		tmp_member_data = {
			avatar = data_member["avatar"],
			username = data_member["username"],
			id = data_member["userid"]
		}
		returnData["member_info"] = tmp_member_data
		
	end
		
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(returnData))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end
elseif data_arr[1+prefix_pos] == "topic" and data_arr[2+prefix_pos] == "all" then
	
	local namecache = ""
	local topic_id  = -1
	local not_id = -1

	if type(ngx.var['arg_size']) ~= "nil" then
		if tonumber(ngx.var['arg_size']) > 0 then
			size = tonumber(ngx.var['arg_size'])
		else
			size = 10
		end
	else
		size = 10
	end
	
	if type(ngx.var['arg_sortby']) ~= "nil" then
		if ngx.var['arg_sortby'] == "cr_date" then
			sort = "topic_id"
		elseif ngx.var['arg_sortby'] == "view" then
			sort = "views"
		elseif ngx.var['arg_sortby'] == "id" then
			sort = "topic_id"
		else
			sort = "topic_id"
		end
	else
		sort = "topic_id"
	end

	if type(ngx.var['arg_topicid']) ~= "nil" and sort == "topic_id" then
		topic_id = tonumber(ngx.var['arg_topicid'])
		namecache = "topic_all_after-topic-" .. topic_id .. "_" .. size .. "_id"
		page = 1
	elseif type(ngx.var['arg_topicid']) ~= "nil" and sort == "views" then
		topic_id = tonumber(ngx.var['arg_topicid'])
		namecache = "topic_all_after-topic-" .. topic_id .. "_" .. size .. "_views"
		
		col = db:get_col("topic")
		topic_data, err = red:get("topic_" .. topic_id)
		if not topic_data then
			ngx.say("failed to get: ", err)
			return
		end
		
		if topic_data == ngx.null then
			topic_data = col:find_one({topic_id = tonumber(topic_id)})
			if type(topic_data) ~= "nil" then
				topic_data["_id"] = nil
				topic_data["cr_date"] =  {
					sec = math.floor(topic_data["cr_date"] / 1000),
					usec = math.floor(math.fmod(topic_data["cr_date"],1000))*1000
				}
				if type(topic_data["picture"]) == "string" then 
					topic_data["picture"] = "http://memberapi.kapook.com/uploads/" .. topic_data["picture"]
				else
					topic_data["picture"] = "http://member.kapook.com/assets/images/bg-test.jpg"
				end
				set_cache_redis("topic_" .. topic_data["topic_id"],time_center,cjson.encode(topic_data))
			end
		else
			topic_data = cjson.decode(topic_data)
		end
		
		if type(topic_data) ~= "nil" then
			views = topic_data["views"]
		else
			views = 0
			topic_data = ngx.null
		end
		page = 1
	else
		if type(ngx.var['arg_page']) ~= "nil" then
			if tonumber(ngx.var['arg_page']) > 0 then
				page = tonumber(ngx.var['arg_page'])
			else
				page = 1
			end
		else
			page = 1
		end
		namecache = "topic_all_" .. size .. "_" .. page .. "_" .. sort
	end
	
	if type(ngx.var['arg_notid']) ~= "nil" then
		not_id = tonumber(ngx.var['arg_notid'])
		namecache = namecache .. "_not-" .. ngx.var['arg_notid']
	end

	local isClear = false

	if type(ngx.var['arg_clear']) ~= "nil" then
		if ngx.var['arg_clear'] == "1" then
			isClear = true
		end
	end
	
	if isClear == false then
		returnData, err = red:get(namecache)
		
		if not returnData then
			ngx.say("failed to get: ", err)
			return
		end
	else
		returnData = ngx.null
	end
	
	if returnData == ngx.null then
		
		local query 		= 	{}
		local list_data		=	{}
		seek_pos = 0
			
		if type(topic_data) ~= "nil" then
			
			query["views"] = views
					
			fullquery = {}
			fullquery["query"] = query
			fullquery["orderby"] = {}
			fullquery["orderby"][0] = {}
			fullquery["orderby"][0]["views"] = -1
			fullquery["orderby"][1] = {}
			fullquery["orderby"][1]["topic_id"] = -1
					
			cursor_id,r,t = col:query( fullquery,{},0,10000,{} )
			n = col:count(query)

			for index, item in pairs(r) do
				
				item["_id"] = nil
				item["cr_date"] =  {
					sec = math.floor(item["cr_date"] / 1000),
					usec = math.floor(math.fmod(item["cr_date"],1000))*1000
				}
				if type(item["picture"]) == "string" then 
					item["picture"] = "http://memberapi.kapook.com/uploads/" .. item["picture"]
				else
					item["picture"] = "http://member.kapook.com/assets/images/bg-test.jpg"
				end
				set_cache_redis("topic_" .. item["topic_id"],time_center,cjson.encode(item))
					
				if item["topic_id"] == tonumber(topic_id) then
					seek_pos = seek_pos + 1
					break
				end
				seek_pos = seek_pos + 1
			end
			
		end
		
		query 		= 	{}
		
		if topic_id > 0 then
			if sort == "views" then
				query["views"] = {}
				query["views"]["$lte"] = views
			else
				query["topic_id"] = {}
				query["topic_id"]["$lt"] = topic_id
			end
		end
		
		if not_id > 0 then
			query["topic_id"] = {}
			query["topic_id"]["$ne"] = not_id
		end
		
		fullquery = {}
		fullquery["query"] = query
		fullquery["orderby"] = {}
		fullquery["orderby"][0] = {}
		fullquery["orderby"][0][sort] = -1
		if sort == "views" then
			fullquery["orderby"][1] = {}
			fullquery["orderby"][1]["topic_id"] = -1
		end
		
		col = db:get_col("topic")
		cursor_id,r,t = col:query( fullquery,{},((page-1)*size)+seek_pos,size,{} )
		n = col:count(query)
		last_id = -1

		returnData = {}
		for index, item in pairs(r) do
			item["_id"] = nil
			item["cr_date"] =  {
				sec = math.floor(item["cr_date"] / 1000),
				usec = math.floor(math.fmod(item["cr_date"],1000))*1000
			}
			if type(item["picture"]) == "string" then 
				item["picture"] = "http://memberapi.kapook.com/uploads/" .. item["picture"]
			else
				item["picture"] = "http://member.kapook.com/assets/images/bg-test.jpg"
			end
			set_cache_redis("topic_" .. item["topic_id"],time_center,cjson.encode(item))
			
			data_member, err = red:get("member_" .. tonumber(item["m_id"]))
			if not data_member then
				ngx.say("failed to get: ", err)
				return
			end
			if data_member == ngx.null then
				data_member = get_kapi_member(tonumber(item["m_id"]))
				set_cache_redis("member_" .. tonumber(item["m_id"]),time_center,cjson.encode(data_member))
			else
				data_member = cjson.decode(data_member)
			end
			tmp_member_data = {
				avatar = data_member["avatar"],
				username = data_member["username"],
				id = data_member["userid"]
			}
			item["member_info"] = tmp_member_data
			
			list_data[index] = item
			last_id = item["topic_id"]
		end
		
		returnData = {
			status_code = 200,
			total_rows = n,
			total_pages = math.ceil(n/size),
			paged = page,
			rows = n,
			data = list_data,
			--query = fullquery
		}
		
		if last_id > 0 then
			returnData["next"] = "http://memberapi2.kapook.com/topic/all?topicid=" .. last_id .. "&size=" .. size
			if sort == "views" then
				returnData["next"] = returnData["next"] .. "&sortby=view"
			end
		else
			returnData["next"] = false
		end
		
		set_cache_redis(namecache,time_center,cjson.encode(returnData))
		
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
	else
		returnData = cjson.decode(returnData)
		returnData["time_cache"] = red:ttl(namecache)
		returnData["is_cache"] = true
		returnData["name_cache"] = namecache
	end
		
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(returnData))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end
elseif data_arr[1+prefix_pos] == "topic" and string.len(data_arr[2+prefix_pos]) > 0 and string.len(data_arr[3+prefix_pos]) > 0 then

	-- data_arr[1+prefix_pos] : topic
	-- data_arr[2+prefix_pos] : m_id
	-- data_arr[3+prefix_pos] : topic_id
	
	local isClear = false
	local time_exist = -1
	local namecache = ""
	local return_data = {}
	
	if tonumber(data_arr[2+prefix_pos]) == nil and tonumber(data_arr[3+prefix_pos]) == nil then
		returnData = {
			status_code = 400,
			description = "input is not numberic."
		}
	else
		----------------- topic -----------------
		namecache = "topic_" .. data_arr[3+prefix_pos]
		if type(ngx.var['arg_clear']) ~= "nil" then
			if ngx.var['arg_clear'] == "1" then
				isClear = true
			end
		end
		
		if isClear == false then
			topic_data, err = red:get(namecache)
			
			if not topic_data then
				ngx.say("failed to get: ", err)
				return
			end
		else
			topic_data = ngx.null
		end

		if topic_data == ngx.null then
			col = db:get_col("topic")
			topic_data = col:find_one({ topic_id=tonumber(data_arr[3+prefix_pos]) , m_id=tonumber(data_arr[2+prefix_pos]) })
			if type(topic_data) ~= "nil" then
				topic_data["_id"] = nil
				topic_data["cr_date"] =  {
					sec = math.floor(topic_data["cr_date"] / 1000),
					usec = math.floor(math.fmod(topic_data["cr_date"],1000))*1000
				}
				if type(topic_data["picture"]) == "string" then
					topic_data["picture"] = "http://memberapi.kapook.com/uploads/" .. topic_data["picture"]
				else
					topic_data["picture"] = "http://member.kapook.com/assets/images/bg-test.jpg"
				end
				set_cache_redis(namecache,time_center,cjson.encode(topic_data))
				time_exist = time_center
			else
				topic_data = ngx.null
			end
			return_data["is_cache"] = false
		else
			time_exist = red:ttl(namecache)
			topic_data = cjson.decode(topic_data)
			return_data["name_cache"] = namecache
			return_data["time_cache"] = time_exist
			return_data["is_cache"] = true
		end
		return_data["topic"] = topic_data

		-------------- list of content --------------
		namecache = "listcontent_topic_" .. data_arr[3+prefix_pos]
		local list_data		=	{}
		
		if time_exist > 0 then
			
			if isClear == false then
				content_data, err = red:get(namecache)
				if not content_data then
					ngx.say("failed to get: ", err)
					return
				end
			else
				content_data = ngx.null
			end
			
			if content_data == ngx.null then

				local query 		= 	{}

				query["topic_id"] = tonumber(data_arr[3+prefix_pos])
				
				fullquery = {}
				fullquery["query"] = query
				fullquery["orderby"] = {}
				fullquery["orderby"]["content_id"] = -1
				
				col = db:get_col("content")
				cursor_id,rs,t = col:query( fullquery,{},0,10000 )
				n = col:count(query)

				list_data = {}
				for index, item in pairs(rs) do
					item["_id"] = nil
					item["cr_date"] =  {
						sec = math.floor(item["cr_date"] / 1000),
						usec = math.floor(math.fmod(item["cr_date"],1000))*1000
					}
					--item["picture"] = "http://memberapi.kapook.com/uploads/" .. item["picture"]
					set_cache_redis("content_" .. item["content_id"],time_center,cjson.encode(item))
					list_data[index] = item
				end

				set_cache_redis(namecache,time_center,cjson.encode({data=list_data,size=n}))
				
			else
				content_data = cjson.decode(content_data)
				n = content_data["size"]
				list_data = content_data["data"]
				return_data["content_namecache"] = namecache
				return_data["content_time_cache"] = red:ttl(namecache)
			end
			return_data["topic"]["contents"] = n
			return_data["content"] = list_data
		end
		
		-------------- member data --------------
		data_member, err = red:get("member_" .. tonumber(data_arr[2+prefix_pos]))
		if not data_member then
			ngx.say("failed to get: ", err)
			return
		end
		if data_member == ngx.null then
			data_member = get_kapi_member(tonumber(data_arr[2+prefix_pos]))
			set_cache_redis("member_" .. tonumber(data_arr[2+prefix_pos]),time_center,cjson.encode(data_member))
		else
			data_member = cjson.decode(data_member)
		end
		tmp_member_data = {
			avatar = data_member["avatar"],
			username = data_member["username"],
			id = data_member["userid"]
		}
		return_data["member_info"] = tmp_member_data
	end
	
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(return_data))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end
elseif data_arr[1+prefix_pos] == "member" and string.len(data_arr[2+prefix_pos]) > 0 then
	data_member, err = red:get("member_" .. tonumber(data_arr[2+prefix_pos]))
	if not data_member then
		ngx.say("failed to get: ", err)
		return
	end
	if data_member == ngx.null then
		data_member = get_kapi_member(tonumber(data_arr[2+prefix_pos]))
		set_cache_redis("member_" .. tonumber(data_arr[2+prefix_pos]),time_center,cjson.encode(data_member))
	else
		data_member = cjson.decode(data_member)
	end
	ngx.say(cjson.encode(data_member))
else
	returnData = {
		status_code = 400,
		description = "input is incorrect."
	}
	ngx.say(cjson.encode(returnData))
end

