-----------------------CJSON-----------------------
local cjson = require "cjson"
-----------------------MongoDB-----------------------
local mongo = require "resty.mongol"
local conn = nil
local db = nil

function get_conn_mongo()
	local conn = mongo:new()
	conn:set_timeout(1000)
	local ok, err = conn:connect("192.168.1.189",27017)
	if not ok then
		ngx.log(ngx.ERR, "error: ", err)
		ngx.exit(500)
	end
	return conn
end

-----------------------Function-----------------------

function json_say(data,callback)
	if type(callback) == "string" then
		return callback .. "(" .. cjson.encode(data) .. ")"
	else
		return cjson.encode(data)
	end
end

function explode(delimiter, text)
	local list = {}
	local pos = 1
	if string.find("", delimiter, 1) then
		error("delimiter matches empty string!")
	end
	local i = 0
	while 1 do
		local first, last = string.find(text, delimiter, pos)
		if first then
			list[i] = string.sub(text, pos, first-1)
			i = i +1
			pos = last+1
		else
			list[i] = string.sub(text, pos)
			break
		end
	end
	return list
end

-----------------------End of Function-----------------------

local data_arr	=	explode('/',ngx.var.uri)

if data_arr[2] == "reset_data" then
	local x = os.clock()
	
	ngx.header["Content-Type"] = "application/json; charset=utf-8"
	
	local callback_str = nil
	if type(ngx.var['arg_callback']) ~= "nil" then
		callback_str = ngx.var['arg_callback']
	end

	local present_single = 0
	local present_multiple = 0
	local used_point = 0
	
	local quota_playside_single		=	3
	local quota_playside_multi		=	1
	local qouta_playside_multi_min	=	2
	local qouta_playside_multi_max	=	6
	
	local max_user = 1000
	
	conn = get_conn_mongo()
	db = conn:new_db_handle ( "football_test" )
	
	local col_member	= 	db:get_col("football_member")
	local col_game		= 	db:get_col("football_game")
	
	for i=1,max_user do
		local query 		= 	{}
		query["$set"]		=	{ id = i, point = 2200 }
		--Member
		col_member:update({ id = i },query,1)
		--Game
		local listGame 		= 	{}
		listGame[0]			=	{
			match_id = 1,
			status = 1,
			team_win = 1
	   }
		query["$set"]		=	{
			id = i,
			date_play = os.date("%Y-%m-%d",tmpTime),
			listGame = listGame,
			user_id = i,
			status = 1,
			point = 2200,
			point_return = 0,
			multiple = 1.0,
			timestamp = os.date("%Y-%m-%d %H:%M:%S"),
			match_id = 1
		}
		local n, err = col_game:update({ id = i },query,1)
	end
	ngx.say(string.format("elapsed time: %.3f\n", os.clock() - x))
	return
elseif data_arr[2] == "caculate_game" then
	return
else
	ngx.say("TEst1.0.1.2")
end