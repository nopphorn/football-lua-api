-- CJSON
local cjson = require "cjson"
-- MONGODB
local mongo = require "resty.mongol"
local col = nil
-- HTTP
local http = require "resty.http"
local httpc	=	nil
-- MONGO BSON
local bson = require('resty.mongol.bson')
local get_utc_date = bson.get_utc_date
--REDIS
local redis = require "resty.redis"
local red = nil

function get_redis()
	local red = redis:new()
	red:set_timeout(1000) -- 1 sec
	ok, err = red:connect("192.168.1.78", 6379)
	if not ok then
		ngx.say("connect failed: "..err)
	end
	red:select(9)
	return red
end

function get_col_mongodb()
	conn = mongo:new()
	conn:set_timeout(1000)
	ok, err = conn:connect("192.168.1.189",27017)
	if not ok then
		ngx.say("connect failed1: "..err)
	end
	local db = conn:new_db_handle ( "football" )
	local col = db:get_col("football_news")
	return col
end

function log_db(command,action)
	local log_file,err = io.open("/var/luacode/footballapi.kapook.com/log_db/" .. os.date("%Y-%m-%d") .. ".txt" , "a+")
	local date_log = os.date("%Y-%m-%d %H:%M:%S")
	local tmp_data = { timestamp = date_log , command = command , action = action , url = ngx.var.uri }
	log_file:write(cjson.encode(tmp_data) .. "\n")
	log_file:close()
end

function explode(delimiter, text)
	local list = {};
	local pos = 1
	if string.find("", delimiter, 1) then
		-- We'll look at error handling later!
		error("delimiter matches empty string!")
	end
	--ngx.say(text)
	local i = 0
	while 1 do
		local first, last = string.find(text, delimiter, pos)
		--ngx.say (first, last)
		if first then
			list[i] = string.sub(text, pos, first-1)
			i = i +1
			pos = last+1
		else
			--table.insert(list, string.sub(text, pos))
			list[i] = string.sub(text, pos)
			break
		end
	end
	--ngx.say(list[1])
	return list
end

function implode(delimiter, array)
	local isFirst = true
	local return_str = ""
	for i in pairs(array) do
		if isFirst then
			isFirst = false
		else
			return_str = return_str .. ","
		end
		return_str = return_str .. array[i]
    end
	return return_str
end

function teamNews(list_team,size,page,sort,not_id)
	
	if page <= 0 then
		page = 1
	end
	
	if size <=0 then
		size = 12
	end
	
	local namecache = "newsteam_" .. list_team .. "_" .. size .. "_" .. page .. "_" .. sort .. "_" .. not_id
	local isClear = false

	if type(ngx.var['arg_clear']) ~= "nil" then
		if ngx.var['arg_clear'] == "1" then
			isClear = true
		end
	end
	
	if isClear == false then
		if red == nil then
			red = get_redis()
		end
		returnData, err = red:get( namecache )
		
		if not returnData then
			ngx.say("failed to get: ", err)
			return
		end
	else
		returnData = ngx.null
	end
	
	if returnData == ngx.null then
		list_team	=	explode(',',list_team)

		local query 	= 	{}
		local listNews	=	{}
		local tmpData 	= 	{}
		
		query["$or"] = {}
		tmpData["extra.kapookfootball_News_CountryOtherKeyword3"] = "0"
		query["$or"][0]	= tmpData
		--[[
		for i in pairs(list_team) do
			local tmpData 	= 	{}
			tmpData["extra.kapookfootball_News_CountryOtherKeyword3"] = list_team[i]
			query["$or"][i+1]	= tmpData
		end]]--
		
		query["$or"][1]	= {}
		query["$or"][1]["extra.kapookfootball_News_CountryOtherKeyword3"] = {}
		query["$or"][1]["extra.kapookfootball_News_CountryOtherKeyword3"]["$in"] = list_team
		
		if not_id > 0 then
			query["$and"] = {}
			query["$and"][0] = {}
			query["$and"][0]["news_id"] = {}
			query["$and"][0]["news_id"]["$ne"] = not_id
		end
		
		query["public"] = 1
		
		fullquery = {}
		fullquery["query"] = query
		fullquery["orderby"] = {}
		fullquery["orderby"][sort] = -1
		
		if col == nil then
			col = get_col_mongodb()
		end
		local cursor_id,r,t = col:query( fullquery,{},(page-1)*size,size,{})
		log_db("query","teamNews")
		--n = col:count(query)
		n = 36

		for index, item in pairs(r) do
			local thumbnail = ""
			local picture = ""
			local URL = ""
	
			if item['extra']['kapookfootball_News_URL'] == '' then 
				URL = 'http://football.kapook.com/news-' .. item['news_id']
			else
				URL = item['extra']['kapookfootball_News_URL']
			end
		
			if string.len(item['picture']['thumbnail']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					thumbnail = "http://football.kapook.com/cms/upload/thumbnail/"..item['picture']['thumbnail']['src']
				else
					thumbnail = "http://football.kapook.com/cms/uploadnews/news/thumbnail/"..item['picture']['thumbnail']['src']
				end
			end
			
			if string.len(item['picture']['large']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					picture = "http://football.kapook.com/cms/upload/large/"..item['picture']['large']['src']
				else
					picture = "http://football.kapook.com/cms/uploadnews/news/picture/"..item['picture']['large']['src']
				end
			end
		
			listNews[index] = {
				_id = item["news_id"],
				content_id = item["news_id"],
				zone_id = "",
				portal_id = 27,
				subject = item["detail"][0]["subject"],
				title = item["detail"][0]["title"],
				thumbnail = thumbnail,
				picture = picture,
				link = URL,
				CreateDate = item["cd"] / 1000,
				LastUpdate = item["ld"] / 1000,
				Views = item["stat"]["view"]
			}
		end
		
		returnData = {
			country_news = {
				name_th = "ทั้งหมด",
				name_en = "All"
			},
			status_code = 200,
			total_rows = n,
			total_pages = math.ceil(n/size),
			paged = page,
			rows = n,
			data = listNews,
			query = fullquery
		}
		if red == nil then
			red = get_redis()
		end
		local ok, err = red:set(namecache,cjson.encode(returnData))
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
		ok, err = red:expire(namecache,300)
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
	else
		if red == nil then
			red = get_redis()
		end
		returnData = cjson.decode(returnData)
		returnData["time_cache"] = red:ttl(namecache)
		returnData["is_cache"] = true
		returnData["name_cache"] = namecache
	end
	
	return returnData

end

ngx.header["Content-Type"] = "application/json; charset=utf-8"

local data_arr	=	explode('/',ngx.var.uri)

if data_arr[2] == "id" and tonumber(data_arr[3]) ~= nil then

	local isClear = false

	if type(ngx.var['arg_clear']) ~= "nil" then
		if ngx.var['arg_clear'] == "1" then
			isClear = true
		end
	end
	
	if isClear == false then
		if red == nil then
			red = get_redis()
		end
		return_data, err = red:get("news_" .. data_arr[3])
		
		if not return_data then
			ngx.say("failed to get: ", err)
			return
		end
	else
		return_data = ngx.null
	end
	
	if return_data == ngx.null then
		if col == nil then
			col = get_col_mongodb()
		end
		r = col:find_one({news_id=tonumber(data_arr[3]),public=1})
		log_db("find_one","search by news_id")
		if type(r) ~= "nil" then
			r["_id"] = nil
			
			-- CONTENT 1 --
			if string.len(r["detail"][0]['content']) == 0 then
				
				if r['news_id'] <= 21632 then
					path_content = "uploadnews/news/htmlfiles/"
				else
					path_content = "upload/content/".. (r['news_id']%999) .."/"
				end
				
				if httpc == nil then
					httpc = http.new()
				end
			
				local res, err = httpc:request_uri("http://202.183.165.189/cms/"..path_content..r['extra']['kapookfootball_News_HTMLFileName'], {
					method = "POST",
					body = "a=1&b=2",
					headers = {
						["Content-Type"] = "application/x-www-form-urlencoded",
					}
				})
				
				if res then
					r["detail"][0]['content'] = res.body
				end
			end
			
			-- CONTENT 2 --
			if type(r['extra']['kapookfootball_News_HTMLFileName2']) ~= "nil" and r['news_id'] > 21632 then
			
				path_content = "upload/content/".. (r['news_id']%999) .."/"
					
				if httpc == nil then
					httpc = http.new()
				end
				
				local res, err = httpc:request_uri("http://202.183.165.189/cms/"..path_content..r['extra']['kapookfootball_News_HTMLFileName2'], {
					method = "POST",
					body = "a=1&b=2",
					headers = {
						["Content-Type"] = "application/x-www-form-urlencoded",
					}
				})
					
				if res.status == 200 then
					r["detail"][0]['content_2'] = res.body
				else
					r["detail"][0]['content_2'] = ""
				end
			else
				r["detail"][0]['content_2'] = ""
			end
			
			if string.len(r['picture']['thumbnail']['src']) ~= 0 then
				if tonumber(data_arr[3]) > 21632 then
					r['picture']['thumbnail']['src'] = "http://football.kapook.com/cms/upload/thumbnail/"..r['picture']['thumbnail']['src']
				else
					r['picture']['thumbnail']['src'] = "http://football.kapook.com/cms/uploadnews/news/thumbnail/"..r['picture']['thumbnail']['src']
				end
			end

			if string.len(r['picture']['large']['src']) ~= 0 then
				if tonumber(data_arr[3]) > 21632 then
					r['picture']['large']['src'] = "http://football.kapook.com/cms/upload/large/"..r['picture']['large']['src']
				else
					r['picture']['large']['src'] = "http://football.kapook.com/cms/uploadnews/news/picture/"..r['picture']['large']['src']
				end
			end
			
			if type(r['picture']['og']) == "nil" then
				r['picture']['og'] = {}
				r['picture']['og']['src'] = ""
			end
			
			if tonumber(data_arr[3]) > 21632 and string.len(r['picture']['og']['src']) ~= 0 then
				r['picture']['og']['src'] = "http://football.kapook.com/cms/upload/og/"..r['picture']['og']['src']
			else
				r['picture']['og']['src'] = r['picture']['large']['src']
			end
			
			if string.len(r['social']['facebook']['title']) == 0 then
				r['social']['facebook']['title'] = r['detail'][0]['subject']
			end
			
			if string.len(r['social']['facebook']['desc']) == 0 then
				r['social']['facebook']['desc'] = r['detail'][0]['title']
			end
			
			local attr_arr = {}
			attr_arr["extra_kapookfootball_News_Country"] = r["extra"]["kapookfootball_News_Country"]
			attr_arr["kapookfootball_News_CountryOtherKeyword3"] = r["extra"]["kapookfootball_News_CountryOtherKeyword3"]
			
			team_news		=	teamNews(implode(",",r["extra"]["kapookfootball_News_CountryOtherKeyword3"]),5,1,"news_id",tonumber(data_arr[3]))
			relate_news		=	team_news["data"]
			
			if string.len(r["extra"]["kapookfootball_News_URL"]) > 0 then
				link_news		=	r["extra"]["kapookfootball_News_URL"]
				isRedirect		= 	true
			else
				link_news		=	"http://football.kapook.com/news-"..data_arr[3]
				isRedirect		= 	false
			end

			return_data		=	{
				news_id = r["news_id"],
				rowContent = { {
					subject 	= r["detail"][0]["subject"],
					title 		= r["detail"][0]["title"],
					question 	= r["detail"][0]["question"],
					content 	= r["detail"][0]["content"],
					content2	= r["detail"][0]["content_2"],
					link 		= link_news,
					isRedirect	= isRedirect
				} },
				Views = r["stat"]["view"],
				CreateDate = r["cd"] / 1000,
				LastUpdate = r["ld"] / 1000,
				VDOPremium = r["extra"]["kapookfootball_News_VDO_Premium"],
				picture = {
					thumb_l = {
						src = r['picture']['large']['src'],
						width = r['picture']['large']['width'],
						height = r['picture']['large']['height']
					},
					thumb_s = {
						src = r['picture']['thumbnail']['src'],
						width = r['picture']['thumbnail']['width'],
						height = r['picture']['thumbnail']['height']
					}
				},
				portal = {
					content_id = r["news_id"],
					portal_id = 27,
					zone_id = cjson.null
				},
				attr = attr_arr,
				relate = relate_news,
				seo = {
					fb = {
						title = r['detail'][0]['subject'],
						des = r['detail'][0]['title'],
						img = r['picture']['og']['src']
					},
					meta = {
						title = r['social']['facebook']['title'],
						des = r['social']['facebook']['desc'],
						tag = r['social']['seo']['keyword'],
						thumnail = r['picture']['large']['src'],
						truehits = cjson.null
					}
				}
			}
			if red == nil then
				red = get_redis()
			end
			ok, err = red:set("news_" .. data_arr[3],cjson.encode(return_data))
			if not ok then
				ngx.say("failed to set : ", err)
				return
			end
			ok, err = red:expire("news_" .. data_arr[3],1800)
			if not ok then
				ngx.say("failed to set : ", err)
				return
			end
		end
	else
		if red == nil then
			red = get_redis()
		end
		return_data = cjson.decode(return_data)
		return_data["time_cache"] = red:ttl("news_" .. data_arr[3])
		return_data["is_cache"] = true
	end
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(return_data))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end
elseif data_arr[2] == "team" and string.len(data_arr[3]) > 0 then
	if tonumber(data_arr[4]) == nil then
		data_arr[4] = 1
	end
	if type(ngx.var['arg_size']) ~= "nil" then
		if tonumber(ngx.var['arg_size']) > 0 then
			size = tonumber(ngx.var['arg_size'])
		else
			size = 12
		end
	else
		size = 12
	end
	
	if type(ngx.var['arg_sortby']) ~= "nil" then
		if ngx.var['arg_sortby'] == "cd" then
			sort = "cd"
		elseif ngx.var['arg_sortby'] == "view" then
			sort = "stat.view"
		else
			sort = "news_id"
		end
	else
		sort = "news_id"
	end
	
	list_news	=	teamNews(data_arr[3],size,tonumber(data_arr[4]),sort,-1)
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(returnData))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end
	
elseif data_arr[2] == "country" and string.len(data_arr[3]) > 0 then
	listCountry	= {}
	
	listCountry["1"] = {name_th = 'อังกฤษ',name_en = 'England'}
	listCountry["2"] = {name_th = 'อิตาลี',name_en = 'Italy'}
	listCountry["3"] = {name_th = 'สเปน',name_en = 'Spain'}
	listCountry["4"] = {name_th = 'เยอรมัน',name_en = 'Germany'}
	listCountry["6"] = {name_th = 'ยูฟ่า แชมเปี้ยนลีก',name_en = 'UEFA Champions League'}
	listCountry["8"] = {name_th = 'ไทย',name_en = 'Thai'}
	listCountry["10"] = {name_th = 'ผรั่งเศส',name_en = 'France'}
	listCountry["11"] = {name_th = 'ยูโรป้า ลีก',name_en = 'UEFA Europa League'}
	listCountry["12"] = {name_th = 'เดอะ แชมเปี้ยนชิพ',name_en = 'The Championship'}
	listCountry["13"] = {name_th = 'ตลาดซื้อขายนักเตะ',name_en = 'Transfer'}
	listCountry["_all"] = {name_th = 'ทั้งหมด',name_en = 'All'}
	
	if type(listCountry[data_arr[3]]) == "nil" then
		if type(ngx.var['arg_callback']) ~= "nil" then
			ngx.say(ngx.var['arg_callback'].."(")
		end
		ngx.say(cjson.encode({status_code=520,error_msg="country value is not valid"}))
		if type(ngx.var['arg_callback']) ~= "nil" then
			ngx.say(")")
		end
		return
	end
	
	if tonumber(data_arr[4]) == nil then
		page = 1
	else
		page = tonumber(data_arr[4])
		if page<=0 then
			page = 1
		end
	end
	
	if type(ngx.var['arg_size']) ~= "nil" then
		if tonumber(ngx.var['arg_size']) > 0 then
			size = tonumber(ngx.var['arg_size'])
		else
			size = 12
		end
	else
		size = 12
	end
	
	if type(ngx.var['arg_sortby']) ~= "nil" then
		if ngx.var['arg_sortby'] == "cd" then
			sort = "cd"
		elseif ngx.var['arg_sortby'] == "view" then
			sort = "stat.view"
		else
			sort = "news_id"
		end
	else
		sort = "news_id"
	end
	
	local namecache = "newsconutry_" .. data_arr[3] .. "_" .. size .. "_" .. page .. "_" .. sort
	local isClear = false

	if type(ngx.var['arg_clear']) ~= "nil" then
		if ngx.var['arg_clear'] == "1" then
			isClear = true
		end
	end
	
	if isClear == false then
		if red == nil then
			red = get_redis()
		end
		returnData, err = red:get(namecache)
		
		if not returnData then
			ngx.say("failed to get: ", err)
			return
		end
	else
		returnData = ngx.null
	end
	
	if returnData == ngx.null then

		local query 	= 	{}
		local listNews	=	{}
		
		query["extra.kapookfootball_News_Country"] = data_arr[3]
		query["public"] = 1
		
		fullquery = {}
		fullquery["query"] = query
		fullquery["orderby"] = {}
		fullquery["orderby"][sort] = -1
		
		if col == nil then
			col = get_col_mongodb()
		end
		local cursor_id,r,t = col:query( fullquery,{},(page-1)*size,size,{})
		log_db("query","search by extra.kapookfootball_News_Country")
		--n = col:count(query)
		n = 36

		returnData = {}
		for index, item in pairs(r) do
			local thumbnail = ""
			local picture = ""
			local URL = ""
	
			if item['extra']['kapookfootball_News_URL'] == '' then 
				URL = 'http://football.kapook.com/news-' .. item['news_id']
			else
				URL = item['extra']['kapookfootball_News_URL']
			end
		
			if string.len(item['picture']['thumbnail']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					thumbnail = "http://football.kapook.com/cms/upload/thumbnail/"..item['picture']['thumbnail']['src']
				else
					thumbnail = "http://football.kapook.com/cms/uploadnews/news/thumbnail/"..item['picture']['thumbnail']['src']
				end
			end
			
			if string.len(item['picture']['large']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					picture = "http://football.kapook.com/cms/upload/large/"..item['picture']['large']['src']
				else
					picture = "http://football.kapook.com/cms/uploadnews/news/picture/"..item['picture']['large']['src']
				end
			end
		
			listNews[index] = {
				_id = item["news_id"],
				content_id = item["news_id"],
				zone_id = "",
				portal_id = 27,
				subject = item["detail"][0]["subject"],
				title = item["detail"][0]["title"],
				thumbnail = thumbnail,
				picture = picture,
				link = URL,
				CreateDate = item["cd"] / 1000,
				LastUpdate = item["ld"] / 1000,
				Views = item["stat"]["view"]
			}
		end
		
		returnData = {
			country_news = listCountry[data_arr[3]],
			status_code = 200,
			total_rows = n,
			total_pages = math.ceil(n/size),
			paged = page,
			rows = n,
			data = listNews,
			query = fullquery
		}
		if red == nil then
			red = get_redis()
		end
		local ok, err = red:set(namecache,cjson.encode(returnData))
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
		ok, err = red:expire(namecache,900)
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
	else
		if red == nil then
			red = get_redis()
		end
		returnData = cjson.decode(returnData)
		returnData["time_cache"] = red:ttl(namecache)
		returnData["is_cache"] = true
		returnData["name_cache"] = namecache
	end
		
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(returnData))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end

elseif data_arr[2] == "tag" and string.len(data_arr[3]) > 0 then

	listCup	= {}
	local tmpData1 = {}
	local tmpData2 = {}
	
	listCup["AFF"] = {name_th = 'เอเอฟเอฟ ซูซุกิ คัพ',name_en	= 'AFF Suzuki Cup',name_short = 'aff'}
	listCup["euro2016"] = {name_th = 'ฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016',name_en	= 'UEFA Euro 2016',name_short = 'euro2016'}

	if data_arr[3] == "aff" then
		data_arr[3] = "AFF"
	end

	if type(listCup[data_arr[3]]) ~= "nil" then
		cup_data = listCup[data_arr[3]]
	else
		cup_data = { test = 2222 }
	end
	
	if tonumber(data_arr[4]) == nil then
		page = 1
	else
		page = tonumber(data_arr[4])
	end
	
	if type(ngx.var['arg_size']) ~= "nil" then
		if tonumber(ngx.var['arg_size']) > 0 then
			size = tonumber(ngx.var['arg_size'])
		else
			size = 12
		end
	else
		size = 12
	end
	
	if type(ngx.var['arg_sortby']) ~= "nil" then
		if ngx.var['arg_sortby'] == "cd" then
			sort = "cd"
		elseif ngx.var['arg_sortby'] == "view" then
			sort = "stat.view"
		else
			sort = "news_id"
		end
	else
		sort = "news_id"
	end
	
	local namecache = "newstag_" .. data_arr[3] .. "_" .. size .. "_" .. page .. "_" .. sort
	local isClear = false

	if type(ngx.var['arg_clear']) ~= "nil" then
		if ngx.var['arg_clear'] == "1" then
			isClear = true
		end
	end
	
	if isClear == false then
		if red == nil then
			red = get_redis()
		end
		returnData, err = red:get(namecache)
		
		if not returnData then
			ngx.say("failed to get: ", err)
			return
		end
	else
		returnData = ngx.null
	end
	
	if returnData == ngx.null then

		local query 	= 	{}
		local listNews	=	{}
		
		query["$or"] = {}
		
		tmpData1["extra.kapookfootball_News_CountryOtherKeyword"] = data_arr[3]
		query["$or"][0]	= tmpData1
		
		tmpData2["extra.kapookfootball_News_CountryOtherKeyword2"] = data_arr[3]
		query["$or"][1]	= tmpData2
		
		query["public"] = 1
		
		fullquery = {}
		fullquery["query"] = query
		fullquery["orderby"] = {}
		fullquery["orderby"][sort] = -1
		
		if col == nil then
			col = get_col_mongodb()
		end
		local cursor_id,r,t = col:query( fullquery,{},(page-1)*size,size,{} )
		log_db("query","search by CountryOtherKeyword")
		--n = col:count(query)
		n = 36

		returnData = {}
		for index, item in pairs(r) do
			local thumbnail = ""
			local picture = ""
			local URL = ""
	
			if item['extra']['kapookfootball_News_URL'] == '' then 
				URL = 'http://football.kapook.com/news-' .. item['news_id']
			else
				URL = item['extra']['kapookfootball_News_URL']
			end
		
			if string.len(item['picture']['thumbnail']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					thumbnail = "http://football.kapook.com/cms/upload/thumbnail/"..item['picture']['thumbnail']['src']
				else
					thumbnail = "http://football.kapook.com/cms/uploadnews/news/thumbnail/"..item['picture']['thumbnail']['src']
				end
			end
			
			if string.len(item['picture']['large']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					picture = "http://football.kapook.com/cms/upload/large/"..item['picture']['large']['src']
				else
					picture = "http://football.kapook.com/cms/uploadnews/news/picture/"..item['picture']['large']['src']
				end
			end
		
			listNews[index] = {
				_id = item["news_id"],
				content_id = item["news_id"],
				zone_id = "",
				portal_id = 27,
				subject = item["detail"][0]["subject"],
				title = item["detail"][0]["title"],
				thumbnail = thumbnail,
				picture = picture,
				link = URL,
				CreateDate = item["cd"] / 1000,
				LastUpdate = item["ld"] / 1000,
				Views = item["stat"]["view"]
			}
		end
		
		returnData = {
			cup_news = cup_data,
			status_code = 200,
			total_rows = n,
			total_pages = math.ceil(n/size),
			paged = page,
			rows = n,
			data = listNews,
			query = fullquery
		}
		if red == nil then
			red = get_redis()
		end
		local ok, err = red:set(namecache,cjson.encode(returnData))
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
		ok, err = red:expire(namecache,900)
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
	else
		if red == nil then
			red = get_redis()
		end
		returnData = cjson.decode(returnData)
		returnData["time_cache"] = red:ttl(namecache)
		returnData["is_cache"] = true
		returnData["name_cache"] = namecache
	end
	
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(returnData))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end
	
elseif data_arr[2] == "top10" then

	if tonumber(data_arr[3]) == nil then
		page = 1
	else
		page = tonumber(data_arr[3])
	end
	
	if type(ngx.var['arg_size']) ~= "nil" then
		if tonumber(ngx.var['arg_size']) > 0 then
			size = tonumber(ngx.var['arg_size'])
		else
			size = 12
		end
	else
		size = 12
	end
	
	if type(ngx.var['arg_sortby']) ~= "nil" then
		if ngx.var['arg_sortby'] == "cd" then
			sort = "cd"
		elseif ngx.var['arg_sortby'] == "view" then
			sort = "stat.view"
		else
			sort = "news_id"
		end
	else
		sort = "news_id"
	end
	
	local namecache = "newstop10_" .. size .. "_" .. page .. "_" .. sort
	local isClear = false

	if type(ngx.var['arg_clear']) ~= "nil" then
		if ngx.var['arg_clear'] == "1" then
			isClear = true
		end
	end
	
	if isClear == false then
		if red == nil then
			red = get_redis()
		end
		returnData, err = red:get(namecache)
		
		if not returnData then
			ngx.say("failed to get: ", err)
			return
		end
	else
		returnData = ngx.null
	end
	
	if returnData == ngx.null then

		local query 	= 	{}
		local listNews	=	{}
		
		query["extra.kapookfootball_News_CountryOtherKeyword2"] = "top10"
		query["public"] = 1
		
		fullquery = {}
		fullquery["query"] = query
		fullquery["orderby"] = {}
		fullquery["orderby"][sort] = -1
		
		if col == nil then
			col = get_col_mongodb()
		end
		local cursor_id,r,t = col:query( fullquery,{},(page-1)*size,size,{})
		log_db("query","search by top10")
		-- n = col:count(query)
		n = 36

		returnData = {}
		for index, item in pairs(r) do
			local thumbnail = ""
			local picture = ""
			local URL = ""
	
			if item['extra']['kapookfootball_News_URL'] == '' then 
				URL = 'http://football.kapook.com/news-' .. item['news_id']
			else
				URL = item['extra']['kapookfootball_News_URL']
			end
		
			if string.len(item['picture']['thumbnail']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					thumbnail = "http://football.kapook.com/cms/upload/thumbnail/"..item['picture']['thumbnail']['src']
				else
					thumbnail = "http://football.kapook.com/cms/uploadnews/news/thumbnail/"..item['picture']['thumbnail']['src']
				end
			end
			
			if string.len(item['picture']['large']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					picture = "http://football.kapook.com/cms/upload/large/"..item['picture']['large']['src']
				else
					picture = "http://football.kapook.com/cms/uploadnews/news/picture/"..item['picture']['large']['src']
				end
			end
		
			listNews[index] = {
				_id = item["news_id"],
				content_id = item["news_id"],
				zone_id = "",
				portal_id = 27,
				subject = item["detail"][0]["subject"],
				title = item["detail"][0]["title"],
				thumbnail = thumbnail,
				picture = picture,
				link = URL,
				CreateDate = item["cd"] / 1000,
				LastUpdate = item["ld"] / 1000,
				Views = item["stat"]["view"]
			}
		end
		
		returnData = {
			country_news = {name_th = 'ทั้งหมด',name_en = 'All'},
			status_code = 200,
			total_rows = n,
			total_pages = math.ceil(n/size),
			paged = page,
			rows = n,
			data = listNews,
			query = fullquery
		}
		if red == nil then
			red = get_redis()
		end
		local ok, err = red:set(namecache,cjson.encode(returnData))
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
		ok, err = red:expire(namecache,3600)
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
		
	else
		if red == nil then
			red = get_redis()
		end
		returnData = cjson.decode(returnData)
		returnData["time_cache"] = red:ttl(namecache)
		returnData["is_cache"] = true
		returnData["name_cache"] = namecache
	end
	
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(returnData))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end
	
elseif data_arr[2] == "tophit" then

	if tonumber(data_arr[3]) == nil then
		page = 1
	else
		page = tonumber(data_arr[3])
	end
	
	if type(ngx.var['arg_size']) ~= "nil" then
		if tonumber(ngx.var['arg_size']) > 0 then
			size = tonumber(ngx.var['arg_size'])
		else
			size = 12
		end
	else
		size = 12
	end
	
	local namecache = "newstophit_" .. size .. "_" .. page
	local isClear = false

	if type(ngx.var['arg_clear']) ~= "nil" then
		if ngx.var['arg_clear'] == "1" then
			isClear = true
		end
	end
	
	if isClear == false then
		if red == nil then
			red = get_redis()
		end
		returnData, err = red:get(namecache)
		
		if not returnData then
			ngx.say("failed to get: ", err)
			return
		end
	else
		returnData = ngx.null
	end
	
	if returnData == ngx.null then

		local query 	= 	{}
		local listNews	=	{}
		local mongoDate = 	get_utc_date((ngx.time()-(60*60*24*3))*1000)
		
		query["cd"] = {}
		query["cd"]["$gte"] = mongoDate
		
		query["public"] = 1
		
		fullquery = {}
		fullquery["query"] = query
		fullquery["orderby"] = {}
		fullquery["orderby"]["stat.view"] = -1
		
		if col == nil then
			col = get_col_mongodb()
		end
		local cursor_id,r,t = col:query( fullquery,{},(page-1)*size,size,{})
		log_db("query","search by tophit")
		-- n = col:count(query)
		n = 36

		returnData = {}
		for index, item in pairs(r) do
			local thumbnail = ""
			local picture = ""
			local URL = ""
	
			if item['extra']['kapookfootball_News_URL'] == '' then 
				URL = 'http://football.kapook.com/news-' .. item['news_id']
			else
				URL = item['extra']['kapookfootball_News_URL']
			end
		
			if string.len(item['picture']['thumbnail']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					thumbnail = "http://football.kapook.com/cms/upload/thumbnail/"..item['picture']['thumbnail']['src']
				else
					thumbnail = "http://football.kapook.com/cms/uploadnews/news/thumbnail/"..item['picture']['thumbnail']['src']
				end
			end
			
			if string.len(item['picture']['large']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					picture = "http://football.kapook.com/cms/upload/large/"..item['picture']['large']['src']
				else
					picture = "http://football.kapook.com/cms/uploadnews/news/picture/"..item['picture']['large']['src']
				end
			end
		
			listNews[index] = {
				_id = item["news_id"],
				content_id = item["news_id"],
				zone_id = "",
				portal_id = 27,
				subject = item["detail"][0]["subject"],
				title = item["detail"][0]["title"],
				thumbnail = thumbnail,
				picture = picture,
				link = URL,
				CreateDate = item["cd"] / 1000,
				LastUpdate = item["ld"] / 1000,
				Views = item["stat"]["view"]
			}
		end
		
		returnData = {
			country_news = {name_th = 'ทั้งหมด',name_en = 'All'},
			status_code = 200,
			total_rows = n,
			total_pages = math.ceil(n/size),
			paged = page,
			rows = n,
			data = listNews,
			query = fullquery
		}
		if red == nil then
			red = get_redis()
		end
		local ok, err = red:set(namecache,cjson.encode(returnData))
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
		if red == nil then
			red = get_redis()
		end
		ok, err = red:expire(namecache,900)
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
	else
		if red == nil then
			red = get_redis()
		end
		returnData = cjson.decode(returnData)
		returnData["time_cache"] = red:ttl(namecache)
		returnData["is_cache"] = true
		returnData["name_cache"] = namecache
	end
	
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(returnData))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end
	
elseif data_arr[2] == "list" then
	if tonumber(data_arr[3]) == nil then
		page = 1
	else
		page = tonumber(data_arr[3])
	end
	
	if type(ngx.var['arg_size']) ~= "nil" then
		if tonumber(ngx.var['arg_size']) > 0 then
			size = tonumber(ngx.var['arg_size'])
		else
			size = 12
		end
	else
		size = 12
	end
	
	if type(ngx.var['arg_sortby']) ~= "nil" then
		if ngx.var['arg_sortby'] == "cd" then
			sort = "cd"
		elseif ngx.var['arg_sortby'] == "view" then
			sort = "stat.view"
		else
			sort = "news_id"
		end
	else
		sort = "news_id"
	end
	
	local namecache = "newslist_" .. size .. "_" .. page .. "_" .. sort
	local isClear = false

	if type(ngx.var['arg_clear']) ~= "nil" then
		if ngx.var['arg_clear'] == "1" then
			isClear = true
		end
	end
	
	if isClear == false then
		if red == nil then
			red = get_redis()
		end
		returnData, err = red:get(namecache)
		
		if not returnData then
			ngx.say("failed to get: ", err)
			return
		end
	else
		returnData = ngx.null
	end
	
	if returnData == ngx.null then

		local query 	= 	{}
		local listNews	=	{}
		
		query["public"] = 1
		
		fullquery = {}
		fullquery["query"] = query
		fullquery["orderby"] = {}
		fullquery["orderby"][sort] = -1
		
		if col == nil then
			col = get_col_mongodb()
		end
		local cursor_id,r,t = col:query( fullquery,{},(page-1)*size,size,{})
		log_db("query","list a news")
		--n = col:count(query)
		n = 36

		returnData = {}
		for index, item in pairs(r) do
			local thumbnail = ""
			local picture = ""
			local URL = ""
	
			if item['extra']['kapookfootball_News_URL'] == '' then 
				URL = 'http://football.kapook.com/news-' .. item['news_id']
			else
				URL = item['extra']['kapookfootball_News_URL']
			end
		
			if string.len(item['picture']['thumbnail']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					thumbnail = "http://football.kapook.com/cms/upload/thumbnail/"..item['picture']['thumbnail']['src']
				else
					thumbnail = "http://football.kapook.com/cms/uploadnews/news/thumbnail/"..item['picture']['thumbnail']['src']
				end
			end
			
			if string.len(item['picture']['large']['src']) ~= 0 then
				if tonumber(item["news_id"]) > 21632 then
					picture = "http://football.kapook.com/cms/upload/large/"..item['picture']['large']['src']
				else
					picture = "http://football.kapook.com/cms/uploadnews/news/picture/"..item['picture']['large']['src']
				end
			end
		
			listNews[index] = {
				_id = item["news_id"],
				content_id = item["news_id"],
				zone_id = "",
				portal_id = 27,
				subject = item["detail"][0]["subject"],
				title = item["detail"][0]["title"],
				thumbnail = thumbnail,
				picture = picture,
				link = URL,
				CreateDate = item["cd"] / 1000,
				LastUpdate = item["ld"] / 1000,
				Views = item["stat"]["view"]
			}
		end
		
		returnData = {
			country_news = {name_th = 'ทั้งหมด',name_en = 'All'},
			status_code = 200,
			total_rows = n,
			total_pages = math.ceil(n/size),
			paged = page,
			rows = n,
			data = listNews,
			query = fullquery
		}
		if red == nil then
			red = get_redis()
		end
		local ok, err = red:set(namecache,cjson.encode(returnData))
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
		ok, err = red:expire(namecache,900)
		if not ok then
			ngx.say("failed to set : ", err)
			return returnData
		end
		
	else
		if red == nil then
			red = get_redis()
		end
		returnData = cjson.decode(returnData)
		returnData["time_cache"] = red:ttl(namecache)
		returnData["is_cache"] = true
		returnData["name_cache"] = namecache
	end
	
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode(returnData))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end
--[[elseif data_arr[2] == "update_viewer" then
	local file,err = io.open("/var/luacode/footballapi.kapook.com/data/test.txt","a+")
	if type(data_arr[3]) == "nil" or type(data_arr[4]) == "nil" then
		file:close()
		if type(ngx.var['arg_callback']) ~= "nil" then
			ngx.say(ngx.var['arg_callback'].."(")
		end
		ngx.say(cjson.encode({isSuccess = false}))
		if type(ngx.var['arg_callback']) ~= "nil" then
			ngx.say(")")
		end
		return
	end
	if type(data_arr[5]) ~= "nil" then
		file:write(data_arr[3] .. "," .. data_arr[4] .. " " .. data_arr[5])
		file:write("\n")
	end
	file:write(data_arr[3] .. "," .. data_arr[4] .. " 0")
	file:write("\n")
	file:close()
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(ngx.var['arg_callback'].."(")
	end
	ngx.say(cjson.encode({isSuccess = true}))
	if type(ngx.var['arg_callback']) ~= "nil" then
		ngx.say(")")
	end]]--
elseif data_arr[2] == "clear_cache_news" then
	if red == nil then
		red = get_redis()
	end
	for i=1,100000 do
		red:del("news_" .. i)
	end
	ngx.say("Clear Cache News.")
end